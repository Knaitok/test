package info.itline.mega.ui;

import info.itline.mega.net.DeviceId;
import info.itline.mega.net.MacAddress;
import java.util.HashMap;
import java.util.Map;

public class PasswordStorage {
    
    private PasswordStorage() {
        passwords = new HashMap<>();
    }
    
    public static PasswordStorage getStorage() {
        if (instance == null) {
            instance = new PasswordStorage();
        }
        return instance;
    }
    
    public boolean havePassword(DeviceId s, PasswordType t) {
        MacAddress a = s.getMacAddress();
        if (!passwords.containsKey(a)) {
            return false;
        }
        return passwords.get(a)[t.getId()] != null;
    }
    
    public Integer getPassword(DeviceId s, PasswordType t) {
        MacAddress a = s.getMacAddress();
        if (!passwords.containsKey(a)) {
            return null;
        }
        return passwords.get(a)[t.getId()];
    }
    
    public void setPassword(DeviceId s, PasswordType t, Integer password) {
        MacAddress a = s.getMacAddress();
        Integer[] ps;
        if (!passwords.containsKey(a)) {
            ps = new Integer[PasswordType.values().length];
            passwords.put(a, ps);
        }
        else {
            ps = passwords.get(a);
        }
        ps[t.getId()] = password;
    }
    
    public void removePassword(DeviceId s, PasswordType t) {
        MacAddress a = s.getMacAddress();
        if (!passwords.containsKey(a)) {
            return;
        }
        passwords.get(a)[t.getId()] = null;
    }

    public void removeUnusedPasswords(Map<MacAddress, DeviceId> devs) {
        for (MacAddress mac: passwords.keySet()) {
            if (!devs.containsKey(mac)) {
                passwords.remove(mac);
            }
        }
    }
    
    private final Map<MacAddress, Integer[]> passwords;
    private static PasswordStorage instance;
}

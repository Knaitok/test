package info.itline.mega.ui;

public enum PasswordType {
    
    ADMIN(0),
    USER(1);
    
    private PasswordType(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    private int id;
}
package info.itline.mega.ui;

import info.itline.mega.net.Client;
import info.itline.mega.net.DeviceId;
import info.itline.mega.net.TimeParser;
import info.itline.mega.net.TimerConfig;
import info.itline.mega.net.TimerConfigField;
import info.itline.mega.net.TimerState;
import info.itline.mega.net.TimerManager;
import static javafx.beans.binding.Bindings.when;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import static info.itline.mega.ui.TimeValidator.*;
import java.util.logging.Logger;
import javafx.scene.control.CheckBox;
import javafx.stage.Window;

public class GenericTimerController {

    public GenericTimerController(short timerIdx, TextField timeInput,
            Button startButton, Button pauseButton, Button stopButton,
            Label currentValue, CheckBox countReverse, Window host,
            boolean controllAllTimers) {
        this.timerIdx = timerIdx;
        this.timeInput = timeInput;
        this.startButton = startButton;
        this.pauseButton = pauseButton;
        this.stopButton = stopButton;
        this.currentValue = currentValue;
        this.countReverseCheckbox = countReverse;
        this.host = host;
        this.controlAllTimers = controllAllTimers;

        stopButton.setVisible(false);

        TimerManager.getInstance().registerTimer(timerIdx, c -> {
            currentValue.setText(TimeParser.formatTime(c.getCurrentValue() / 100));
            isTimerRunning.set(c.getState() == TimerState.RUNNING.getValue());
            if (!isTimerRunning.get() && controllAllTimers) {
                TimerManager.getInstance().stopRegisteredTimers(null, null);
            }
        });

        startButton.disableProperty().bind(containsValidTime(timeInput).not());
        pauseButton.textProperty().bind(when(isTimerRunning).then("Пауза").otherwise("Продолжить"));

        errorHandler = (e, c) -> {
            PopupHelper.showPopup(e.getDescription(), host);
        };

        startButton.setOnAction(t -> {
            int[] startStop = getStartAndStopValues();
            int start = startStop[0];
            int stop = startStop[1];

            log.info("Timer started from " + start + " to " + stop);
            //         Client.getInstance().setTimerStartAndStopValues(targetDeviceId,
            //            timerIdx, start, stop, null, errorHandler);
            //         Client.getInstance().setTimerCurrentValue(targetDeviceId, timerIdx,
            //            start, null, errorHandler);
            TimerConfig c = new TimerConfig(start * 100, start * 100, stop * 100, TimerConfig.ENABLE_SIREN,
                    TimerState.STOPPED.getValue(), TimerConfigField.ALL.getValue());
            Client.getInstance().setTimerConfig(targetDeviceId, timerIdx, c, r -> {
                if (controllAllTimers) {
                    TimerManager.getInstance().startRegisteredTimers(p -> isTimerRunning.set(true), errorHandler);
                }
                else {
                    Client.getInstance().startTimer(targetDeviceId, timerIdx,
                            p -> isTimerRunning.set(true), errorHandler);
                }
            }, errorHandler);
        });

        pauseButton.setOnAction(t -> {
            if (isTimerRunning.get()) {
                if (controllAllTimers) {
                    TimerManager.getInstance().stopRegisteredTimers(
                            p -> isTimerRunning.set(false), errorHandler);
                }
                else {
                    Client.getInstance().pauseTimer(targetDeviceId, timerIdx,
                            p -> isTimerRunning.set(false), errorHandler);
                }
            }
            else {
                if (controllAllTimers) {
                    TimerManager.getInstance().startRegisteredTimers(
                            p -> isTimerRunning.set(true), errorHandler);
                }
                else {
                    Client.getInstance().startTimer(targetDeviceId, timerIdx,
                            p -> isTimerRunning.set(true), errorHandler);
                }
            }
        });

        stopButton.setOnAction(t -> {
            if (controllAllTimers) {
                TimerManager.getInstance().stopRegisteredTimers(
                        p -> isTimerRunning.set(false), errorHandler);
            }
            else {
                Client.getInstance().resetTimer(targetDeviceId, timerIdx,
                        getStartAndStopValues()[0],
                        p -> isTimerRunning.set(false),
                        errorHandler);
            }
        });
    }

    private int[] getStartAndStopValues() {
        int[] result = new int[2];
        int time = TimeParser.parseTime(timeInput.getText());
        if (countReverseCheckbox.isSelected()) {
            result[0] = time;
            result[1] = 0;
        }
        else {
            result[1] = time;
            result[0] = 0;
        }
        return result;
    }

    public void setStartTime(int t) {
        timeInput.setText(TimeParser.formatTime(t / 100));
    }
    
    public static void setTargetDeviceId(DeviceId id) {
        targetDeviceId = id;
        TimerManager.getInstance().setTargetDeviceId(id);
    }

    public static DeviceId getTargetDeviceId() {
        return targetDeviceId;
    }

    public short getTimerIdx() {
        return timerIdx;
    }

    private final short timerIdx;
    private final TextField timeInput;
    private final Button startButton, pauseButton, stopButton;
    private final Label currentValue;
    private final CheckBox countReverseCheckbox;
    private final Window host;
    private final boolean controlAllTimers;

    private final Client.OnError errorHandler;

    private final BooleanProperty isTimerRunning = new SimpleBooleanProperty();

    private static DeviceId targetDeviceId;

    private static final Logger log = Logger.getLogger(GenericTimerController.class.getName());
}

package info.itline.mega.ui;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.effect.BoxBlur;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageBuilder;
import javafx.stage.StageStyle;
import javafx.stage.Window;

public class FxDialogHelper {
    
    public static Stage createModalStage(Pane rootNode, final Window owner, 
            StageStyle style, String title) {
        Scene scene = new Scene(rootNode);
        
        final Stage stage = StageBuilder.create()
                .scene(scene)
                .style(style)
                .title(title)
                .build();
        
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initOwner(owner);
        
        if (owner == null) {
            return stage;
        }
        
        final Parent ownerRoot = owner.getScene().getRoot();
        
        stage.setOnShowing(
                t -> {
                    BoxBlur blur = new BoxBlur(3.0, 3.0, 1);
                    blur.setInput(ownerRoot.getEffect());
                    ownerRoot.setEffect(blur);
                });
        
        stage.setOnShown(
                t -> {
                    stage.setX(owner.getX() + owner.getWidth() / 2 - stage.getWidth() / 2);
                    stage.setY(owner.getY() + owner.getHeight() / 2 - stage.getHeight() / 2);
                });
        
        stage.setOnHiding(
                t -> {
                    ownerRoot.setEffect(((BoxBlur) ownerRoot.getEffect()).getInput());
                });
        
        return stage;
    }
    
    public static void showErrorDialog(Window owner, String message) {
        ErrorDialogController.showAndWait(owner, message);
    }
}

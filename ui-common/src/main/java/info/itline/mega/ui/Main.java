package info.itline.mega.ui;

import info.itline.mega.net.DeviceType;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        ApplicationLaunchHelper.launchApplication(this, primaryStage, 
                DeviceType.MEGA_BOARD, false, false, 
                NetworkSettingsController.load(), 
                BrightnessSettingsController.load(),
                DateTimeController.load(),
                AboutController.load());
    }

    public static void main(String[] args) {
        launch(args);
    }
}

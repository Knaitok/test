package info.itline.mega.ui;

import info.itline.mega.net.TimeParser;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TextField;

public class TimeValidator extends BooleanBinding {

    private TimeValidator(StringProperty string) {
        this.string = string;
        bind(string);
    }
    
    public static TimeValidator containsValidTime(TextField f) {
        return new TimeValidator(f.textProperty());
    }
    
    @Override
    protected boolean computeValue() {
        try {
            TimeParser.parseTime(string.get());
            return true;
        }
        catch (IllegalArgumentException e) {
            return false;
        }
    }
    
    private final StringProperty string;
}

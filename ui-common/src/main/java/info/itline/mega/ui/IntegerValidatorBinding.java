package info.itline.mega.ui;

import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.StringExpression;
import javafx.scene.control.TextField;

public class IntegerValidatorBinding extends BooleanBinding {

    private IntegerValidatorBinding(TextField f, long min, long max) {
        this.expr = f.textProperty();
        this.min = min;
        this.max = max;
        bind(expr);
    }

    public static IntegerValidatorBinding inRange(TextField f, int min, int max) {
        return new IntegerValidatorBinding(f, min, max);
    }
    
    public static IntegerValidatorBinding containsByte(TextField f) {
        return new IntegerValidatorBinding(f, Byte.MIN_VALUE, Byte.MAX_VALUE);
    }
    
    public static IntegerValidatorBinding containsUnsignedByte(TextField f) {
        return new IntegerValidatorBinding(f, 0, Byte.MAX_VALUE);
    }
    
    public static IntegerValidatorBinding containsShort(TextField f) {
        return new IntegerValidatorBinding(f, Short.MIN_VALUE, Short.MAX_VALUE);
    }
    
    public static IntegerValidatorBinding containsUnsignedShort(TextField f) {
        return new IntegerValidatorBinding(f, 0, Short.MAX_VALUE);
    }
    
    public static IntegerValidatorBinding containsInteger(TextField f) {
        return new IntegerValidatorBinding(f, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }
    
    public static IntegerValidatorBinding containsUnsignedInteger(TextField f) {
        return new IntegerValidatorBinding(f, 0, Integer.MAX_VALUE);
    }
    
    @Override
    protected boolean computeValue() {
        long i;
        try {
            i = Long.parseLong(expr.getValue());
        } 
        catch (NumberFormatException e) {
            return false;
        }
        if (i < min || i > max) {
            return false;
        }
        return true;
    }
    
    private final StringExpression expr;
    private final long min, max;
}
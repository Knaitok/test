package info.itline.mega.ui;

import info.itline.mega.ui.FxDialogHelper;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Pattern;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.StringExpression;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.StageStyle;
import javafx.stage.Window;

public class PasswordDialogController implements Initializable {

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        okButton.disableProperty().bind(new IsValidPassword(passwordField.textProperty()).not());
        okButton.getParent().setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent t) {
                KeyCode code = t.getCode();
                if (code == KeyCode.ESCAPE) {
                    finish(false);
                }
                else if (code == KeyCode.ENTER && !okButton.isDisabled()) {
                    finish(true);
                }
            }
        });
    }
    
    @FXML
    private void okButtonClicked(MouseEvent t) {
        isApproved = true;
        finish(true);
    }
    
    @FXML
    private void cancelButtonClicked(MouseEvent t) {
        finish(false);
    }
    
    private void finish(boolean isApproved) {
        this.isApproved = isApproved;
        passwordField.getScene().getWindow().hide();
    }
    
    private static class IsValidPassword extends BooleanBinding {

        public IsValidPassword(StringExpression expr) {
            bind(expr);
            this.expr = expr;
        }

        @Override
        protected boolean computeValue() {
            return pattern.matcher(expr.get()).matches();
        }
        
        private StringExpression expr;
        
        private static Pattern pattern = Pattern.compile("[A-D0-9]{4}");
    }
    
    public static int askForPassword(Window dialogOwner) {
        FXMLLoader loader = new FXMLLoader(PasswordDialogController.class.getResource("fxml/PasswordDialog.fxml"));
        Pane root = null;
        PasswordDialogController controller = null;
        try {
            loader.load();
            root = loader.getRoot();
            controller = loader.getController();
        }
        catch (IOException ignore) {
            
        }
        
        FxDialogHelper.createModalStage(root, dialogOwner, StageStyle.UTILITY, "Пароль").showAndWait();
        return controller.isApproved ? Integer.parseInt(controller.passwordField.getText(), 16) : -1;
    }
    
    private boolean isApproved = false;
    
    @FXML
    private PasswordField passwordField;
    @FXML
    private Button okButton, cancelButton;
}

package info.itline.mega.ui;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

public class WaitNotificationController implements Initializable {

    public static Form<WaitNotificationController> load() {
        try {
            FXMLLoader loader = new FXMLLoader(
                    NetworkSettingsController.class.getResource(
                            "fxml/WaitNotification.fxml"));
            loader.load();
            return new Form<>(
                    (WaitNotificationController) loader.getController(), 
                    (Pane) loader.getRoot());
        }
        catch (IOException ignore) {
            throw new RuntimeException(ignore);
        }
    }
    
    public static Stage createModalStage(Window owner) {
        Form<WaitNotificationController> form = load();
        Pane root = form.getRootNode();
        return FxDialogHelper.createModalStage(
                root, owner, StageStyle.UNDECORATED, "");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
}

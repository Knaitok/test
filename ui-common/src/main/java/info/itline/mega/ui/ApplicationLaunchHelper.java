package info.itline.mega.ui;

import info.itline.config.bankboard.ConfigManager;
import info.itline.config.bankboard.Exceptions.ConfigurationParsingFailed;
import info.itline.mega.net.Client;
import info.itline.mega.net.DeviceType;
import java.io.IOException;
import java.util.logging.LogManager;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ApplicationLaunchHelper {

    private ApplicationLaunchHelper() {
    }

    private static void initApplication(Application app, boolean useConfig) {
        ApplicationHolder.theApplication = app;
        setLoggerConfiguration();

        try {
            Client.initialize();
            if (useConfig) {
                ConfigManager.loadConfig();
            }
        }
        catch (IOException | ConfigurationParsingFailed e) {
            String msg;
            if (e.getMessage().contains("already in use")) {
                msg = "Программа для управления уже запущена";
            }
            else {
                msg = "Не удалось запусить программу: " + e.getMessage();
            }
            FxDialogHelper.showErrorDialog(null, msg);
            System.exit(1);
        }
    }

    private static void composeAndShowMainStage(Stage primaryStage, DeviceType deviceType,
            boolean useConfig, boolean askPassword,
            Form<? extends SettingsGroup>... sections) {
        Form<DeviceSelectorController> form = DeviceSelectorController.load();
        DeviceSelectorController ctl = form.getController();
        ctl.setDeviceType(deviceType);
        ctl.setAskPassword(askPassword);
        ctl.setUseConfig(useConfig);
        ctl.setChildren(sections);

        Scene scene = new Scene(form.getRootNode(), 1200, 700);

        Pane root = form.getRootNode();
        Rectangle clip = new Rectangle();
        clip.widthProperty().bind(root.widthProperty());
        clip.heightProperty().bind(root.heightProperty());
        clip.setArcHeight(10);
        clip.setArcWidth(10);

        root.setClip(clip);
        scene.setFill(Color.TRANSPARENT);

        primaryStage.setTitle("Програма для управления табло");
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.setScene(scene);
        primaryStage.show();

        primaryStage.setOnHidden(t -> System.exit(0));

        ctl.searchForDevices();
    }

    public static void launchApplication(Application app, Stage primaryStage,
            DeviceType deviceType, boolean useConfig, boolean askPassword,
            Form<? extends SettingsGroup>... sections) {
        initApplication(app, useConfig);
        composeAndShowMainStage(primaryStage, deviceType,
                useConfig, askPassword, sections);
    }

    private static void setLoggerConfiguration() {
        try {
            LogManager.getLogManager().readConfiguration(ApplicationLaunchHelper.class.getResourceAsStream("logging.properties"));
        }
        catch (IOException e) {
            System.err.println("Could not setup logger configuration: " + e.toString());
        }
    }

    public static boolean IS_CLUSTER_BOARD = false;
}

package info.itline.mega.ui;

import static info.itline.mega.ui.PasswordType.*;

public enum UpdateOperations {
    CHANGE_NETWORK_SETTINGS(ADMIN),
    CHANGE_DATE_TIME_SETTINGS(USER),
    CHANGE_NVRAM_SETTINGS(USER),
    CHANGE_PERSISTENT_STRING(USER),
    CHANGE_TRANSIENT_STRING(USER),
    CHANGE_PERSISTENT_INTEGER(USER),
    CHANGE_TRANSIENT_INTEGER(USER),
    CHANGE_MODE(USER),
    CHANGE_TEMPERATURE_SETTINGS(USER);
    
    private UpdateOperations(PasswordType pt) {
        passwordType = pt;
    }
    
    public PasswordType getPasswordType() {
        return passwordType;
    }
    
    private final PasswordType passwordType;
}

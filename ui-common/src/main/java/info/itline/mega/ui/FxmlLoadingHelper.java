package info.itline.mega.ui;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.Pane;

public class FxmlLoadingHelper {

    private FxmlLoadingHelper() {
    }

    public static <T extends Initializable> Form<T> load(Class<T> clazz, String url) {
        try {
            FXMLLoader loader = new FXMLLoader(clazz.getResource(url));
            loader.load();
            return new Form<>((T) loader.getController(), (Pane) loader.getRoot());
        }
        catch (IOException ignore) {
            throw new RuntimeException(ignore);
        }
    }    
}

package info.itline.mega.ui;

import info.itline.config.cluster.Clusters;
import info.itline.mega.net.Client;
import info.itline.mega.net.DateTimeSettingsPacket;
import info.itline.mega.net.DeviceId;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.stage.Window;

public class DateTimeController extends SettingsGroup implements Initializable {

   public static Form<DateTimeController> load() {
      return FxmlLoadingHelper.load(
         DateTimeController.class, "fxml/DateTimePane.fxml");
   }

   @Override
   public void initialize(URL url, ResourceBundle rb) {

   }

   @Override
   protected void updateView(DeviceId id) {
      if (id != null) {
         Client.getInstance().getDateTime(
            id, p -> updateDateTime(p.getStamp()), null);
      }
      else {
         dateField.setText("");
         timeField.setText("");
      }
   }

   @Override
   public String getGroupName() {
      return "Дата и время";
   }

   @Override
   public String getIconFilename() {
      return "datetime.png";
   }

   @Override
   public Window getWindow() {
      return dateField.getScene().getWindow();
   }

   @Override
   protected UpdateOperations getOperationType() {
      return UpdateOperations.CHANGE_DATE_TIME_SETTINGS;
   }

   private void updateDateTime(int stamp) {
      Calendar c = DateTimeSettingsPacket.convertTimestampToCalendar(stamp);
      dateField.setText(dateFormat.format(c.getTime()));
      timeField.setText((timeFormat.format(c.getTime())));
   }

   @FXML
   private void syncButtonClicked(ActionEvent t) {
      getPasswordAndExecute((password, handler) -> {
         for (DeviceId id : Clusters.getInstance().getClusterOrThisNode(getDeviceId())) {
            Client.getInstance().synchronizeDateTime(
               id,
               p -> {
                  updateDateTime(p.getStamp());
                  defaultReceiveHandler.handle(p);
               },
               handler);
         }
      });
   }

   @FXML
   private TextField dateField, timeField;

   private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy"),
      timeFormat = new SimpleDateFormat("HH:mm:ss");
}

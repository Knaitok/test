package info.itline.mega.ui;

import info.itline.config.cluster.Clusters;
import info.itline.mega.net.Client;
import info.itline.mega.net.DeviceId;
import info.itline.mega.net.MacAddress;
import info.itline.mega.net.NvramSettingsPacket;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Slider;
import javafx.stage.Window;

public class BrightnessSettingsController extends SettingsGroup implements Initializable {

   public static Form<BrightnessSettingsController> load() {
      return FxmlLoadingHelper.load(
         BrightnessSettingsController.class,
         "fxml/BrightnessSettingsPane.fxml");
   }

   @Override
   public void initialize(URL url, ResourceBundle rb) {
      brightnessSlider.setMin(NvramSettingsPacket.MIN_BRIGHTNESS);
      brightnessSlider.setMax(NvramSettingsPacket.MAX_BRIGHTNESS);
      brightnessSlider.setMajorTickUnit(5.0);
   }

   @Override
   protected void updateView(DeviceId id) {
      if (id != null) {
         Client.getInstance().getNvramSettings(
            id, p -> brightnessSlider.setValue(p.getBrightness()), null);
         Client.getInstance().getNvramConfig(id,
            p -> {
               enableAutoBrightness.setSelected(p.getAutoBrightness() != 0);
            },
            null);
      }
   }

   @Override
   public Window getWindow() {
      return brightnessSlider.getScene().getWindow();
   }

   @Override
   public String getGroupName() {
      return "Яркость";
   }

   @Override
   public String getIconFilename() {
      return "brightness.png";
   }

   @Override
   protected UpdateOperations getOperationType() {
      return UpdateOperations.CHANGE_NVRAM_SETTINGS;
   }

   @FXML
   private void saveButtonClicked(ActionEvent t) {
      getPasswordAndExecute((password, handler) -> {
         List<DeviceId> cluster = Clusters.getInstance().getClusterOrThisNode(getDeviceId());
         for (DeviceId id : cluster) {
            Client.getInstance().setNvramSettings(id,
               (int) brightnessSlider.getValue(), defaultReceiveHandler, handler);
            Client.getInstance().getNvramConfig(getDeviceId(),
               p -> {
                  Client.getInstance().setNvramConfig(
                     getDeviceId(), p.getSafe(), p.getMin(), p.getMax(),
                     p.getWatchdogPeriod(),
                     !enableAutoBrightness.isSelected(), defaultReceiveHandler, handler);
               }, handler);
         }
      });
   }

   @FXML
   private Slider brightnessSlider;
   @FXML
   private CheckBox enableAutoBrightness;

   private static final Logger log
      = Logger.getLogger(BrightnessSettingsController.class.getName());
}

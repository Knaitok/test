package info.itline.mega.ui;

import javafx.fxml.Initializable;
import javafx.scene.layout.Pane;

public class Form<I extends Initializable> {

    public Form(I controller, Pane rootNode) {
        this.controller = controller;
        this.rootNode = rootNode;
    }
    
    public I getController() {
        return controller;
    }

    public Pane getRootNode() {
        return rootNode;
    }
    
    private final I controller;
    private final Pane rootNode;
}

package info.itline.config.cluster;

import info.itline.mega.net.DeviceId;
import info.itline.mega.net.DeviceType;
import info.itline.mega.net.IpAddress;
import info.itline.mega.net.MacAddress;
import info.itline.mega.ui.ApplicationLaunchHelper;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Clusters {

   private Clusters() {
      try {
         File f = new File(CONFIG_FILENAME);
         if (!f.isFile()) {
            return;
         }

         Document root = getRootElement();
         NodeList clusterElements = root.getElementsByTagName("cluster");

         for (int i = 0; i < clusterElements.getLength(); i++) {
            Element clusterElement = (Element) clusterElements.item(i);
            NodeList controllers = clusterElement.getElementsByTagName("controller");

            List<DeviceId> controllerList = new ArrayList<>();
            for (int j = 0; j < controllers.getLength(); j++) {
               Element controller = (Element) controllers.item(j);
               String ipText = getInnerTextOfSingularChildElement(controller, "ip");
               String macText = getInnerTextOfSingularChildElement(controller, "mac");
               if (macText == null) {
                  System.out.println("Missing mac");
               }
               try {
                  DeviceId id = new DeviceId(DeviceType.MEGA_BOARD,
                     ipText != null 
                        ? IpAddress.parse(ipText) 
                        : IpAddress.broadcastAddress, 
                     MacAddress.parse(macText), "clusterItem");
                  controllerList.add(id);
               }
               catch (IllegalArgumentException ignore) {
                  ignore.printStackTrace();
               }
            }

            if (!controllerList.isEmpty()) {
               clusters.add(controllerList);
            }
         }
      }
      catch (IOException | SAXException | ParserConfigurationException e) {
         e.printStackTrace();
      }
   }

   private static String getInnerTextOfSingularChildElement(Element elt, String tag) {
      NodeList nodes = elt.getElementsByTagName(tag);
      if (nodes.getLength() != 1) {
         return null;
      }

      Element child = (Element) nodes.item(0);
      return child.getTextContent();
   }

   public List<DeviceId> getClusterByNode(DeviceId nodeAddress) {
      for (List<DeviceId> cluster : clusters) {
         for (DeviceId id : cluster) {
            if (id.getMacAddress().equals(nodeAddress.getMacAddress())) {
               return Collections.unmodifiableList(cluster);
            }
         }
      }

      return null;
   }

   public List<List<DeviceId>> getAllClusters() {
      return clusters;
   }

   public List<DeviceId> getClusterOrThisNode(DeviceId nodeAddress) {
      if (!ApplicationLaunchHelper.IS_CLUSTER_BOARD) {
         return wrapDeviceId(nodeAddress);
      }
      else {
         List<DeviceId> cluster = getClusterByNode(nodeAddress);
         if (cluster != null) {
            return cluster;
         }
         else {
            return wrapDeviceId(nodeAddress);
         }
      }
   }

   private List<DeviceId> wrapDeviceId(DeviceId id) {
      List<DeviceId> result = new ArrayList<>();
      result.add(id);
      return result;
   }

   private static Document getRootElement() throws IOException, SAXException, ParserConfigurationException {
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      return builder.parse(new File(CONFIG_FILENAME));
   }

   public static Clusters getInstance() {
      if (instance == null) {
         instance = new Clusters();
      }

      return instance;
   }

   private List<List<DeviceId>> clusters = new ArrayList<>();
   private static Clusters instance;
   private static final String CONFIG_FILENAME = "clusters.xml";
}

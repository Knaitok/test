package info.itline.config.bankboard;

import info.itline.mega.net.IpAddress;
import info.itline.mega.net.MacAddress;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ConfigManager {
    private ConfigManager(Map<MacAddress, DeviceConfig> devices) {
        this.devices = devices;
    }
    
    public static ConfigManager getManager() 
            throws IllegalStateException {
        if (instance == null) {
            IllegalStateException exc = new IllegalStateException("Конфигурация не загружена");
            log.severe(exc.getMessage());
            throw exc;
        }
        return instance;
    }
    
    public static void loadConfig() throws Exceptions.ConfigurationParsingFailed {
        if (instance == null) {
            instance = readConfig();
        }
    }
    
    public DeviceConfig getConfig(MacAddress address) throws Exceptions.DeviceNotFound {
        DeviceConfig cfg = devices.get(address);
        if (cfg == null)
            throw new Exceptions.DeviceNotFound(address.toString());
        return cfg;
    }
    
    public Collection<DeviceConfig> getAllConfigs() {
        return devices.values();
    }
    
    private static ConfigManager readConfig() 
            throws Exceptions.ConfigurationParsingFailed {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document cfg = builder.parse(new File(CONFIG_FILE_NAME));
            
            Element root = cfg.getDocumentElement();
            NodeList deviceNodes = root.getElementsByTagName(DEVICE_TAG_NAME);
            Map<MacAddress, DeviceConfig> deviceList = new HashMap<>();
            for (int i = 0; i < deviceNodes.getLength(); i++) {
                DeviceConfig dev = readDeviceConfig(deviceNodes.item(i));
                deviceList.put(dev.getMacAddress(), dev);
                log.info("Прочитана информация об устройстве " + 
                        " " + dev.getIpAddress() + " " + dev.getMacAddress());
            }
            
            return new ConfigManager(deviceList);
        }
        catch (IOException | ParserConfigurationException | SAXException exc) {
            Exceptions.ConfigurationParsingFailed res = 
                    new Exceptions.ConfigurationParsingFailed(exc);
            log.severe(res.getMessage());
            throw res;
        }
    }
    
    private static DeviceConfig readDeviceConfig(Node node) 
            throws Exceptions.ConfigurationParsingFailed {
        String data = null;
        MacAddress macAddress = null;
        int digitCount = 0;
        int dotPosition = 0;
        boolean haveCreepingLine = false;
        int creepingLineLength = 0;
        IpAddress ipAddress = null;
        int crossDigitCount = 0;
        int crossDotPosition = 0;
        try {
            macAddress = MacAddress.parse(getAndCheckAttribute(node, MAC_ATTR_NAME));
            data = readAttribute(node, DIGIT_COUNT_ATTR_NAME);
            if(data != null) {
                digitCount = Integer.parseInt(data);
                dotPosition = Integer.parseInt(getAndCheckAttribute(node,
                        DOT_POSITION_ATTR_NAME));
            }
            else {
                digitCount = DeviceConfig.DIGIT_COUNT_MIN;
                dotPosition = DeviceConfig.DOT_POSITION_MIN;
            }
            data = readAttribute(node, HAVE_CREEPING_LINE_ATTR_NAME);
            if(data != null) {
                haveCreepingLine = stringToBoolean(data,
                        HAVE_CREEPING_LINE_ATTR_NAME);
                creepingLineLength = Integer.parseInt(getAndCheckAttribute(node,
                        CREEPING_LINE_LENGTH_ATTR_NAME));
            } else {
                haveCreepingLine = false;
                creepingLineLength = 0;
            }
            data = readAttribute(node, CROSS_DIGIT_COUNT_ATTR_NAME);
            if (data != null) {
                crossDigitCount = Integer.parseInt(data);
                crossDotPosition = Integer.parseInt(getAndCheckAttribute(node,
                        CROSS_DOT_POSITION_ATTR_NAME));
            } else {
                crossDigitCount = DeviceConfig.DIGIT_COUNT_MIN;
                crossDotPosition = DeviceConfig.DOT_POSITION_MIN;
            }
            data = readIpAddress(node);
            if (data != null) {
                ipAddress = IpAddress.parse(data);
            }
        }
        catch (IllegalArgumentException exc) {
            parsingFailed("Недопустимое значение аттрибута: " + exc.getMessage());
        }
        
        List<String> currencyNames = readStringList(node, CURRENCY_TAG_NAME);
        List<String> additionInfo = readStringList(node, ADDITIONAL_INFO_TAG_NAME);
        List<String> crossNames = readStringList(node, CROSS_TAG_NAME);
        
        try {
            return new DeviceConfig(currencyNames, additionInfo, macAddress, 
                    digitCount, dotPosition, haveCreepingLine, creepingLineLength, 
                    ipAddress, crossNames, crossDigitCount, crossDotPosition);
        }
        catch (Exceptions.WrongDeviceSettings exc) {
            parsingFailed("Недопустимые параметры устройства: " + exc.getMessage());
            return null;
        }
    }
    
    private static boolean stringToBoolean(String value, String parameterName)
            throws Exceptions.ConfigurationParsingFailed {
        if (value.equalsIgnoreCase(Boolean.TRUE.toString())) {
            return true;
        }
        else if (value.equalsIgnoreCase(Boolean.FALSE.toString())) {
            return false;
        }
        else {
            parsingFailed("Недопустимое значения для параметра " + parameterName + 
                    ": " + value);
            // dont ever be executed
            return false;
        }
    }
    
    private static List<String> readStringList(Node node, String tag) {
        NodeList nodes = ((Element)node).getElementsByTagName(tag);
        List<String> result = new ArrayList<>();
        for (int i = 0; i < nodes.getLength(); i++) {
            result.add(nodes.item(i).getTextContent());
        }
        return result;
    }
    
    private static String readIpAddress(Node node) 
            throws Exceptions.ConfigurationParsingFailed {
        NodeList ipNode = ((Element) node).getElementsByTagName(SEARCH_BY_IP_TAG_NAME);
        if (ipNode.getLength() == 0) {
            return null;
        }
        return ipNode.item(0).getTextContent();
    }
    
        
    /**
     * Read attribute from the node, if there is no data return null.
     * @param node Node to read data from
     * @param attributeName name of the attribute to search for
     * @return String data or null
     */
    private static String readAttribute(Node node, String attributeName) {
        Node attr = node.getAttributes().getNamedItem(attributeName);
        if (attr == null) {
            return null;
        } else {
            return attr.getNodeValue();
        }
    }
    
    private static String getAndCheckAttribute(Node node, String name) 
            throws Exceptions.ConfigurationParsingFailed {
        Node attr = node.getAttributes().getNamedItem(name);
        checkSyntax(attr == null, "Отсутствует атрибут " + name);
        return attr.getNodeValue();
    }
    
    private static void checkSyntax(boolean isError, String message) 
            throws Exceptions.ConfigurationParsingFailed {
        if (isError) {
            parsingFailed(message);
        }
    }
    
    private static void parsingFailed(String message) 
            throws Exceptions.ConfigurationParsingFailed {
        Exceptions.ConfigurationParsingFailed exc = 
                new Exceptions.ConfigurationParsingFailed(message);
        log.severe(exc.getMessage());
        throw exc;
    }
    
    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder();
        buf.append("[");
        for (DeviceConfig c: devices.values()) {
            buf.append(c);
        }
        buf.append("]");
        return buf.toString();
        
    }
    
    private static final String CONFIG_FILE_NAME = "config.xml";
    
    private Map<MacAddress, DeviceConfig> devices;
    private static ConfigManager instance;
    
    private static final String
            DEVICE_TAG_NAME = "device",
            CURRENCY_TAG_NAME = "currency",
            ADDITIONAL_INFO_TAG_NAME = "additionalInfo",
            SEARCH_BY_IP_TAG_NAME = "searchByIp",
            MAC_ATTR_NAME = "mac",
            DIGIT_COUNT_ATTR_NAME = "digitCount",
            DOT_POSITION_ATTR_NAME = "dotPosition",
            HAVE_CREEPING_LINE_ATTR_NAME = "haveCreepingLine",
            CREEPING_LINE_LENGTH_ATTR_NAME = "creepingLineLength",
            CROSS_DOT_POSITION_ATTR_NAME = "crossDotPosition",
            CROSS_DIGIT_COUNT_ATTR_NAME = "crossDigitCount",
            CROSS_TAG_NAME = "cross";
    
    private static final Logger log = Logger.getLogger(ConfigManager.class.getName());
}

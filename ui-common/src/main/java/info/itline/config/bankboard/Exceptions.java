package info.itline.config.bankboard;

public abstract class Exceptions {
    public static abstract class Base extends Exception {
        public Base(String message) {
            super(message);
        }
        
        public Base(Exception e) {
            super(e);
        }
    }
    
    public static class ConfigurationParsingFailed extends Base {
        public ConfigurationParsingFailed(Exception e) {
            super(DESCRIPTION + ": " + e.getMessage());
        }
        
        public ConfigurationParsingFailed(String message) {
            super(DESCRIPTION + ": " + message);
        }
        
        private static final String DESCRIPTION = "Ошибка при чтении файла конфигурации";
    }
    
    public static class WrongDeviceSettings extends Base {
        public WrongDeviceSettings(Exception e) {
            super(DESCRIPTION + ": " + e.getMessage());
        }

        public WrongDeviceSettings(String message) {
            super(DESCRIPTION + ": " + message);
        }
        private static final String DESCRIPTION = "Недопустимая конфигурация устройства";
    }
    
    public static class DeviceNotFound extends Base {
        public DeviceNotFound(Exception e) {
            super(DESCRIPTION + ": " + e.getMessage());
        }

        public DeviceNotFound(String message) {
            super(DESCRIPTION + ": " + message);
        }
        private static final String DESCRIPTION = "Отсутствует информация об устройстве";
    }
}

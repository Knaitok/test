package info.itline.editor.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public enum Datasource implements Serializable {
    
    NONE                            (0,     1,      1,      "Не определен"),
    RTC                             (1,     1,      4,      "Часы"),
    TEMPERATURE                     (2,     1,      4,      "Температура"),
    ILLUMINATION                    (3,     1,      4,      "Освещенность"),
    PRESSURE                        (4,     1,      4,      "Давление"),
    HUMIDITY                        (5,     1,      4,      "Влажность"),
    RADIATION                       (6,     1,      4,      "Радиация"),
    PERSISTENT_STRING               (7,     2,      1024,   "Энергонез. строка"),
    TRANSIENT_STRING                (8,     32,     128,    "Опер. строка"),
    CONST_STRING                    (9,     32,     128,    "Пост. строка"),
    PERSISTENT_INT                  (10,    256,    4,      "Энергонез. число"),
    TRANSIENT_INT                   (11,    256,    4,      "Опер. число"),
    CONST_INT                       (12,    256,    4,      "Пост. число"),
    TIMER                           (13,    64,     4,      "Таймер"),
    PERSISTENT_IMAGE                (14,    2,      1024,   "Энергонез. изобр."),
    TRANSIENT_IMAGE                 (15,    32,     128,    "Опер. изобр."),
    CONST_IMAGE                     (16,    32,     128,    "Пост. изобр."),
    EECONST                         (17,    16,     2,      "Константа"),
    COLOR_PERSISTENT_IMAGE          (18,    2,      1024,   "Цвет. энергонез. изобр."),
    COLOR_TRANSIENT_IMAGE           (19,    32,     128,    "Цвет. опер. изобр."),
    COLOR_CONST_IMAGE               (20,    32,     128,    "Цвет. конст. изобр.");
    
    private Datasource(int id, int itemCount, int size, String descr) {
        mId = id;
        mItemCount = itemCount;
        mSize = size;
        mDescr = descr;
    }

    public int getId() {
        return mId;
    }
    
    public int getItemCount() {
        return mItemCount;
    }

    public int getSize() {
        return mSize;
    }

    @Override
    public String toString() {
        return mDescr;
    }

    public String getName() {
        return super.toString();
    }

    public static Datasource getById(int id) throws ClassNotFoundException {
        Datasource result = map.get(id);
        if (result == null) {
            throw new ClassNotFoundException();
        }
        return result;
    }
    
    private int mId;
    private int mItemCount;
    private int mSize;
    private String mDescr;

    private static final Map<Integer, Datasource> map = new HashMap<>();
    static {
        for (Datasource d: Datasource.values()) {
            map.put(d.getId(), d);
        }
    }

    public static Datasource[] editableDataSources() {
        return new Datasource[]{
                RTC, PERSISTENT_STRING, TRANSIENT_STRING, PERSISTENT_INT, TRANSIENT_INT, TIMER,
                PERSISTENT_IMAGE, TRANSIENT_IMAGE, EECONST, COLOR_PERSISTENT_IMAGE,
                COLOR_TRANSIENT_IMAGE, COLOR_CONST_IMAGE
        };
    }
}

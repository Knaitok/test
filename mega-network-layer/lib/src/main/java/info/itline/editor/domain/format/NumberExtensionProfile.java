package info.itline.editor.domain.format;

/**
 * How to treat numbers in the number driver.
 */
public enum NumberExtensionProfile {
    PROFILE_NONE(0, "Отсутствует обработка"),
    PROFILE_SIMPLE_POINT(1, "Обработка для числовых полей"),
    PROFILE_MATRIX(2, "Обработка для матричных полей"),
    PROFILE_MATRIX_LEADING_ZEROES(3, "Обработка для матричных полей с 0 впереди");

    public int code;
    public String hint;

    NumberExtensionProfile(int code, String hint) {
        this.code = code;
        this.hint = hint;
    }
}

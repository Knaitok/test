package info.itline.editor.domain.format;

/**
 * Alignment of numbers.
 */
public enum NumberAlignment {
    ALIGN_NONE(0, "Нет выравнивания"),
    ALIGN_LEFT(1, "Выравнивание слева"),
    ALIGN_CENTER(2, "Выравнивание слева"),
    ALIGN_RIGHT(3, "Выравнивание справа");

    public int code;
    public String hint;

    NumberAlignment(int code, String hint) {
        this.code = code;
        this.hint = hint;
    }
}

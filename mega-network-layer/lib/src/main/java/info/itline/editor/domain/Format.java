package info.itline.editor.domain;

import info.itline.editor.domain.format.NumberAlignment;
import info.itline.editor.domain.format.NumberExtensionProfile;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public enum Format implements Serializable {
    
    NONE                (0,     "Не определен",                            0),
    RTC_TIME            (1,     "Время",                                   1),
    RTC_DATE            (2,     "Дата",                                    2),
    RTC_HH_MM           (3,     "ЧЧ:ММ",                                   3),
    RTC_HH_MM_SS        (4,     "ЧЧ:ММ:СС",                                4),
    RTC_MM_SS           (5,     "ММ:СС",                                   5),
    RTC_HH              (6,     "ЧЧ",                                      6),
    RTC_MM              (7,     "ММ",                                      7),
    RTC_SS              (8,     "СС",                                      8),
    RTC_DD_MN           (9,     "ДД:МС",                                   9),
    RTC_DD_MN_YY        (10,    "ДД:МС:ГГ",                                10),
    RTC_DD_MN_YYYY      (11,    "ДД:МС:ГГГГ",                              11),
    RTC_YY              (12,    "ГГ",                                      12),
    RTC_YYYY            (13,    "ГГГГ",                                    13),
    RTC_MN              (14,    "МС",                                      14),
    RTC_DD              (15,    "ДД",                                      15),
    RTC_DOW             (16,    "ДН",                                      16),
    RTC_DOW_MN_DD       (17,    "ДН:МС:ДД",                                17),
    RTC_DOW_MN_DD_YY    (18,    "ДН:ММ:ДД:ГГ",                             18),
    RTC_DOW_MN_DD_YYYY  (19,    "ДН:ММ:ДД:ГГГГ",                           19),
    STR_LEFT            (20,    "Строка по лев. краю",                     1),
    STR_CENTER          (21,    "Строка по центру",                        2),
    STR_RIGHT           (22,    "Строка по прав. краю",                    3),
    STR_LSCROLL         (23,    "Строка по лев. краю с прокр.",            4),
    STR_CSCROLL         (24,    "Строка по центру с прокр.",               5),
    STR_RSCROLL         (25,    "Строка по прав. краю с прокр.",           6),
    STR_LOW_SPEED       (26,    "Бегущая строка с низ. скор.",             7),
    STR_MEDIUM_SPEED    (27,    "Бегущая строка со сред. скор.",           8),
    STR_HIGH_SPEED      (28,    "Бегущая строка с выс. скор.",             9),
    STR_CUSTOM_SPEED    (29,    "Бегущая строка с заданной скор.",         10),
    I_8_L               (30,    ".8 по лев. краю",                         0x41),
    I_88_L              (31,    ".88 по лев. краю",                        0x42),
    I_888_L             (32,    ".888 по лев. краю",                       0x43),
    I_8888_L            (33,    ".8888 по лев. краю",                      0x44),
    I_8_C               (34,    ".8 по центру",                            0x51),
    I_88_C              (35,    ".88 по центру",                           0x52),
    I_888_C             (36,    ".888 по центру",                          0x53),
    I_8888_C            (37,    ".8888 по центру",                         0x54),
    I_8_R               (38,    ".8 по прав. краю",                        0x51),
    I_88_R              (39,    ".88 по прав. краю",                       0x52),
    I_888_R             (40,    ".888 по прав. краю",                      0x53),
    I_8888_R            (41,    ".8888 по прав. краю",                     0x54),
    INT_LEFT            (42,    "Число по лев. краю",                      1),
    INT_CENTER          (43,    "Число по центру",                         2),
    INT_RIGHT           (44,    "Число по прав. краю",                     3),
    INT_RIGHT_ZERO      (45,    "Число по прав. краю с вед. нулями",       4),
    AUTO                (46,    "Автоматически",                           1),
    ILLUMINATION_ADC    (47,    "Освещенность, абс. значение",             1),
    ILLUMINATION_REL    (48,    "Освещенность, день/сумерки/ночь",         2),
    TIMER_SS            (49,    "Таймер, СС",                              1),
    TIMER_SS_MS         (50,    "Таймер, СС:МС",                           2),
    TIMER_MM_SS         (51,    "Таймер, ММ:СС",                           3),
    TIMER_MM_SS_MS      (52,    "Таймер, ММ:СС:МС",                        4),
    TIMER_HH_MM_SS      (53,    "Таймер, ЧЧ:ММ:СС",                        5),
    TIMER_HH_MM_SS_MS   (54,    "Таймер, ЧЧ:ММ:СС:МС",                     6),
    INT_DAY_COUNTER     (55,    "Счетчик дней",                            5),
    INT_DAY_COUNTER_FWD (56,    "Счетчик дней, возр.",                     6),
    INT_DAY_COUNTER_BKW (57,    "Счетчик дней, убыв.",                     7),
    INT_EXT_POINT       (58,    "Число с точкой",                          1),
    INT_EXT_VAL         (59,    "Число",                                   2),
    INT_EXT_VAL_Z       (60,    "Число с ведущими нулями",                 3),
    EECONST_SHOWTIME    (61,    "Время отображения",                       1),
    EECONST_PASS        (62,    "Скрытый пароль",                          2),
    EECONST_PASSV       (63,    "Видимый пароль",                          3),
    NUM_MATRIX_ZEROES_88_88(64, "Матрица, нули, формат 88.88",
            constructFormat(NumberExtensionProfile.PROFILE_MATRIX_LEADING_ZEROES,
                    2, 2)),
    NUM_MATRIX_ZEROES_888_88(65, "Матрица, нули, формат 888.88",
            constructFormat(NumberExtensionProfile.PROFILE_MATRIX_LEADING_ZEROES,
                    3, 2)),
    NUM_MATRIX_ZEROES_88_888(66, "Матрица, нули, формат 88.888",
            constructFormat(NumberExtensionProfile.PROFILE_MATRIX_LEADING_ZEROES,
                    2, 3)),
    NUM_MATRIX_ZEROES_888_888(67, "Матрица, нули, формат 888.888",
            constructFormat(NumberExtensionProfile.PROFILE_MATRIX_LEADING_ZEROES,
                    3, 3)),
    NUM_MATRIX_88_88(68, "Матрица, формат 88.88",
            constructFormat(NumberExtensionProfile.PROFILE_MATRIX, 2, 2)),
    NUM_MATRIX_888_88(69, "Матрица, формат 888.88",
            constructFormat(NumberExtensionProfile.PROFILE_MATRIX, 3, 2)),
    NUM_MATRIX_88_888(70, "Матрица, формат 888.88",
            constructFormat(NumberExtensionProfile.PROFILE_MATRIX, 2, 3)),
    NUM_MATRIX_888_888(71, "Матрица, формат 888.888",
            constructFormat(NumberExtensionProfile.PROFILE_MATRIX, 3, 3)),
    ;

    Format(int id, String repr, int value) {
        mId = id;
        mRepr = repr;
        mValue = value;
    }

    public int getValue() {
        return mValue;
    }
    
    @Override
    public String toString() {
        return mRepr;
    }
    
    public String getName() {
        return super.toString();
    }

    public int getId() {
        return mId;
    }
    
    public static Format[] getAvailableFormatsForDatasource(Datasource d) {
        Format[] result = formatMappings.get(d);
        if (result == null) {
            throw new RuntimeException("Format list is not specified for " + d);
        }
        return result;
    }
    
    public static Format getById(int id) throws ClassNotFoundException {
        Format result = map.get(id);
        if (result == null) {
            throw new ClassNotFoundException();
        }
        return result;
    }
    
    private String mRepr;
    private int mValue;
    private int mId;

    // size in bytes
    public static final int SIZE = 1;
    
    private static final Map<Datasource, Format[]> formatMappings = new HashMap<>();
    private static final Map<Integer, Format> map = new HashMap<>();
    static {
        formatMappings.put(Datasource.NONE, new Format[] {Format.NONE});
        Format[] rtcFormats = new Format[] {
                RTC_TIME,
                RTC_DATE,
                RTC_HH_MM,
                RTC_HH_MM_SS,
                RTC_MM_SS,
                RTC_HH,
                RTC_MM,
                RTC_SS,
                RTC_DD_MN,
                RTC_DD_MN_YY,
                RTC_DD_MN_YYYY,
                RTC_YY,
                RTC_YYYY,
                RTC_MN,
                RTC_DD,
                RTC_DOW,
                RTC_DOW_MN_DD,
                RTC_DOW_MN_DD_YY,
                RTC_DOW_MN_DD_YYYY
        };
        formatMappings.put(Datasource.RTC, rtcFormats);
        
        Format[] timerFormats = new Format[] {
                TIMER_SS         ,
                TIMER_SS_MS      ,
                TIMER_MM_SS      ,
                TIMER_MM_SS_MS   ,
                TIMER_HH_MM_SS   ,
                TIMER_HH_MM_SS_MS
        };
        formatMappings.put(Datasource.TIMER, timerFormats);
        
        Format[] stringFormats = new Format[] { 
                STR_LEFT,
                STR_CENTER,
                STR_RIGHT,
                STR_LSCROLL,
                STR_CSCROLL,
                STR_RSCROLL,
                STR_LOW_SPEED,
                STR_MEDIUM_SPEED,
                STR_HIGH_SPEED,
                STR_CUSTOM_SPEED };
        
        Format[] integerFormats = new Format[] {
                I_8_L,
                I_88_L,
                I_888_L,
                I_8888_L,
                I_8_C,
                I_88_C,
                I_888_C,
                I_8888_C,
                I_8_R,
                I_88_R,
                I_888_R,
                I_8888_R,
                INT_LEFT,
                INT_CENTER,
                INT_RIGHT,
                INT_RIGHT_ZERO,
                INT_DAY_COUNTER,
                INT_DAY_COUNTER_FWD,
                INT_DAY_COUNTER_BKW,
                NUM_MATRIX_ZEROES_88_88,
                NUM_MATRIX_ZEROES_88_888,
                NUM_MATRIX_ZEROES_888_88,
                NUM_MATRIX_ZEROES_888_888,
                NUM_MATRIX_88_88,
                NUM_MATRIX_88_888,
                NUM_MATRIX_888_88,
                NUM_MATRIX_888_888,
        };
        
        formatMappings.put(Datasource.PERSISTENT_STRING, stringFormats);
        formatMappings.put(Datasource.TRANSIENT_STRING, stringFormats);
        formatMappings.put(Datasource.CONST_STRING, stringFormats);
        
        formatMappings.put(Datasource.PERSISTENT_INT, integerFormats);
        formatMappings.put(Datasource.TRANSIENT_INT, integerFormats);
        formatMappings.put(Datasource.CONST_INT, integerFormats);
        
        Format[] autoFormat = new Format[] {AUTO};
        formatMappings.put(Datasource.TEMPERATURE, autoFormat);
        formatMappings.put(Datasource.PRESSURE, autoFormat);
        formatMappings.put(Datasource.HUMIDITY, autoFormat);
        formatMappings.put(Datasource.RADIATION, autoFormat);
        
        formatMappings.put(Datasource.ILLUMINATION, 
                new Format[] {ILLUMINATION_ADC, ILLUMINATION_REL});
        
        Format[] imgFormat = new Format[] { Format.NONE };
        formatMappings.put(Datasource.CONST_IMAGE, imgFormat);
        formatMappings.put(Datasource.TRANSIENT_IMAGE, imgFormat);
        formatMappings.put(Datasource.PERSISTENT_IMAGE, imgFormat);

        for (Format f: Format.values()) {
            map.put(f.getId(), f);
        }
    }

    /**
     * Construct a bit pattern for a number format based on the parameters. It can
     * be used only for number fields.
     * @param profile a number profile to use
     * @param alignment an alignment to use
     * @param digitsAfterPoint how many many digits should be shown after the point (
     * @return
     */
    static int constructFormat(NumberExtensionProfile profile,
                                       NumberAlignment alignment, int digitsAfterPoint) {
        return (profile.code & 0b11) << 6 |
                ((alignment.code & 0b11) << 4) |
                (digitsAfterPoint & 0b1111);
    }

    /**
     * Construct a bit pattern for a number in matrix fields based on the paameters.
     * In can be used only for matrix fields.
     * @param profile the number profile
     * @param digitsBeforePoint how many digits before point should be shown
     * @param digitsAfterPoint how many digits after the point should be shown
     * @return a format that should be shown
     */
    static int constructFormat(NumberExtensionProfile profile, int digitsBeforePoint,
                               int digitsAfterPoint) {
        return ((profile.code & 0b11) << 6) |
                ((digitsBeforePoint & 0b111) << 3) |
                (digitsAfterPoint & 0b111);
    }
}




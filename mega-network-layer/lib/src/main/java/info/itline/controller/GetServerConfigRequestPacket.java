package info.itline.controller;

public class GetServerConfigRequestPacket extends RequestPacket {
    
    public GetServerConfigRequestPacket(DeviceSettings device) {
        super(RequestPacketType.GET_SERVER_CONFIG, device.getType(), 
                device.getMACAddress());
    }
    
    public GetServerConfigRequestPacket(DeviceType type, MACAddress address) {
        super(RequestPacketType.GET_SERVER_CONFIG, type, 
                address);
    }
}

package info.itline.controller;

public abstract class Exceptions {
    public static abstract class Base extends Exception {
        Base(String message) {
            super(message);
        }
    }
    
    public static class OperationFailed extends Base {
        OperationFailed(String operation, String message) {
            super(operation + " не удалось: " + message);
        }

        public OperationFailed(String message) {
            super(message);
        }
    }
    
    public static class WrongPassword extends OperationFailed {
        WrongPassword(String operation) {
            super(operation, DESRIPTION);
        }
        
        private static final String DESRIPTION = "Неправильный пароль";
    }
    
    public static class TimedOut extends OperationFailed {
        public TimedOut(String operation, MACAddress address) {
            super(String.format("Время ожидания истекло (%s, %s)", operation, address));
        }
    } 
    
    public static class InvalidPacket extends Base {
        InvalidPacket(String message) {
            super(description + ": " + message);
       
        }
        
        static final String description = "Некорректный формат пакета";
    }
}

package info.itline.controller;

import com.google.common.io.LittleEndianDataInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import static info.itline.controller.FlashingConst.*;

class FlashingResponsePacket extends ResponsePacket {
    
    FlashingResponsePacket(LittleEndianDataInputStream body) throws Exceptions.InvalidPacket {
        super(body);
        checkPacketType(ResponsePacketType.FLASHING_RESPONSE, logger);
        // checkDeviceType(logger);
        try {
            state = State.getById(body.readShort());
            target = FlashingTarget.getById(body.readShort());
            if (state == null || target == null) {
                throw new Exceptions.InvalidPacket("'State' or 'Target' parameter has invalid value");
            }
            if (state == State.READY) {
                address = body.readInt();
            }
            else if (state == State.RESULT) {
                result = Result.getById(body.readInt());
                if (result == null) {
                    throw new Exceptions.InvalidPacket("'Result' parameter has invalid value");
                }
            }
        }
        catch (IOException exc) {
            handleIOException(exc, logger);
        }
    }
    
    State getState() {
        return state;
    }
    
    FlashingTarget getTarget() {
        return target;
    }
    
    int getMemoryAddress() {
        return address;
    }
    
    Result getResult() {
        return result;
    }
    
    private State state;
    private FlashingTarget target;
    private int address;
    private Result result;
    
    private static final Logger logger = LoggerFactory.getLogger(
            FlashingResponsePacket.class);
}

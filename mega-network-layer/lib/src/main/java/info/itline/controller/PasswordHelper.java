package info.itline.controller;

import java.util.Random;

public class PasswordHelper {
    static int getSalt(int password) {
        int salt = generator.nextInt() & 0xFFFF;
        while (password + salt > 0xFFFF)
            salt /= 2;
        return salt;
    }
    
    static int encryptPassword(int password, int salt) {
        int t = (salt + password) & 0xFFFF;
        return (t ^ salt) & 0xFFFF;
    }
    
    private static Random generator = new Random(); 
}

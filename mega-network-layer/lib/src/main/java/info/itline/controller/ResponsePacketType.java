package info.itline.controller;

import java.util.Map;
import java.util.TreeMap;

public enum ResponsePacketType {
    SEARCH                          (2),
    NETWORK_SETTINGS                (5),
    NVRAM_DATA                      (8),
    DYNAMIC_DATA                    (11),
    DATE_TIME_SETTINGS              (14),
    CREEPING_LINE_SETTINGS          (17),
    SERVER_CONFIG                   (21),
    BAD_PASSWORD                    (200),
    FLASHING_RESPONSE               (101);
    
    private static final Map<Integer, ResponsePacketType> idMapping = 
            new TreeMap<>();
    
    static {
        for (ResponsePacketType t: ResponsePacketType.values())
            idMapping.put(t.getId(), t);
    }
    
    ResponsePacketType(int id) {
        this.id = id;
    }
    
    int getId() {
        return id;
    }

    static ResponsePacketType getById(int id) {
        if (idMapping.containsKey(id))
            return idMapping.get(id);
        else
            return null;
    }
    
    private int id;
}

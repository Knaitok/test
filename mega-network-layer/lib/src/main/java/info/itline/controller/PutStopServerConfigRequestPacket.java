package info.itline.controller;

import java.io.IOException;

public class PutStopServerConfigRequestPacket extends RequestPacket {
    
    public PutStopServerConfigRequestPacket(DeviceSettings device, int password) {
        super(RequestPacketType.PUT_DYNAMIC_DATA, device.getType(), device.getMACAddress());
        this.serverConfig = new StopServerConfig(device.getStopServerConfig());
        this.password = password;
    }

    @Override
    protected PacketBodyStream makeOutputStream() throws IOException {
        PacketBodyStream result = super.makeOutputStream();
        
        result.putInt(serverConfig.getSafe());
        result.putShort(serverConfig.getTempMin());
        result.putShort(serverConfig.getTempMax());
        
        result.putInt(serverConfig.getUpdatePeriod());
        result.putInt(serverConfig.getWatchdogPeriod());
        
        result.putInt(serverConfig.getPort());
        result.putInt(serverConfig.getId());
        result.putInt(serverConfig.getZero());
        
        serverConfig.getGprsAccessPoint().write(result);
        serverConfig.getGprsLogin().write(result);
        serverConfig.getGprsPassword().write(result);
        
        serverConfig.getServerName().write(result);
        serverConfig.getPageOnServer().write(result);
        serverConfig.getHostName().write(result);
        
        result.putPassword(password);
        
        return result;
        
    }
    
    private StopServerConfig serverConfig;
    private int password;
}

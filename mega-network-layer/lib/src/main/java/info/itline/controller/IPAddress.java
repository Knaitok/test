package info.itline.controller;

import com.google.common.io.LittleEndianDataInputStream;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IPAddress extends HWAddress {
    public IPAddress(byte[] bytes) {
        super(bytes);
    }

    public IPAddress(byte[] stream, int offset) {
        super(stream, offset);
    }
    
    IPAddress(IPAddress other) {
        super(other);
    }
    
    IPAddress(LittleEndianDataInputStream stream) throws IOException {
        super(stream);
    }
    
    public static boolean isAddress(String s) {
        Matcher m = IP_PATTERN.matcher(s);
        if (!m.matches())
            return false;
        for (int i = 0; i < ADDRESS_SIZE; i++) {
            if (Integer.parseInt(m.group(i + 1)) > 0xFF) {
                return false;
            }
        }
        return true;
    }
    
    public static IPAddress parse(String s) throws IllegalArgumentException {
        Matcher m = IP_PATTERN.matcher(s);
        IllegalArgumentException exc = 
                new IllegalArgumentException("Строка не является IP-адресом");
        if (!m.matches() || m.groupCount() != ADDRESS_SIZE) {
            throw exc;
        }
        byte[] buf = new byte[ADDRESS_SIZE];
        for (int i = 0; i < m.groupCount(); i++) {
            int v = Integer.parseInt(m.group(i + 1));
            if (v > 255)
                throw new IllegalArgumentException(
                        "Значения байтов IP-адреса не должны быть больше 255");
            buf[i] = (byte) v;
        }
        return new IPAddress(buf);
    }
    
    @Override
    public int getAddressSize() {
        return ADDRESS_SIZE;
    }
    
    @Override
    protected String getAddressPartFormat() {
        return "%d";
    }
    
    @Override
    protected String getAddressSeparatorFormat() {
        return ".";
    }
    
    private static final Pattern IP_PATTERN;
    static {
        String b = "\\s*([0-9]{1,3})\\s*";
        String s = ".";
        IP_PATTERN = Pattern.compile(
                "^" + b + s + b + s + b + s + b +"$");
    }
    
    private static final int ADDRESS_SIZE = 4;
}

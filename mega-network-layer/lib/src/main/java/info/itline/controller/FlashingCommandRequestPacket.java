package info.itline.controller;

import java.io.IOException;
import static info.itline.controller.FlashingConst.*;

class FlashingCommandRequestPacket extends RequestPacket {
    
    FlashingCommandRequestPacket(DeviceSettings settings, Command command, FlashingTarget target) {
        super(RequestPacketType.FLASHING_COMMAND, settings.getType(), 
                settings.getMACAddress());
        this.command = command;
        this.target = target;
    }
    
    @Override
    protected PacketBodyStream makeOutputStream() throws IOException {
        PacketBodyStream res = super.makeOutputStream();
        res.putShort(command.id);
        res.putShort(target.id);
        return res;
    }
    
    private Command command;
    private FlashingTarget target;
}

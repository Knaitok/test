package info.itline.controller;

import com.google.common.io.LittleEndianDataInputStream;
import java.io.IOException;

public class DisplayDurationData extends ArrayData {
    public DisplayDurationData(int[] data) {
        super(data);
    }

    DisplayDurationData(LittleEndianDataInputStream in)
            throws IOException {
        super(in);
    }
    
    DisplayDurationData(DisplayDurationData other) {
       super(other);
    }
    
    @Override
    protected int getSize() {
        return SIZE;
    }
    
    public static final int SIZE = 8;
    public static final int MAX_VALUE = 0xFFFF;
}

package info.itline.controller;

import com.google.common.io.LittleEndianDataInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class CreepingLineSettingsResponsePacket extends ResponsePacket {

    public CreepingLineSettingsResponsePacket(LittleEndianDataInputStream data) throws Exceptions.InvalidPacket {
        super(data);
        checkPacketType(ResponsePacketType.CREEPING_LINE_SETTINGS, logger);
        // checkDeviceType(logger);
        try {
            status = data.readInt() & 0x7FFFFFFF;
            mode = data.readInt() & 0x7FFFFFFF;
        }
        catch (IOException exc) {
            handleIOException(exc, logger);
        }
    }

    public long getStatus() {
        return status;
    }

    public long getMode() {
        return mode;
    }
    
    private long status;
    private long mode;

    private static final Logger logger = LoggerFactory.getLogger(CreepingLineSettingsResponsePacket.class);
}

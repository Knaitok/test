package info.itline.controller;

import com.google.common.io.LittleEndianDataInputStream;
import java.io.IOException;

public abstract class ArrayData {
    ArrayData(int[] data) {
        assert(data.length <= getSize());
        this.data = new int[getSize()];
        System.arraycopy(data, 0, this.data, 0, getSize());
    }

    ArrayData(LittleEndianDataInputStream in)
            throws IOException {
        data = new int[getSize()];
        for (int i = 0; i < getSize(); i++) {
            data[i] = in.readUnsignedShort();
        }
    }
    
    ArrayData(ArrayData other) {
        if (this.getClass() != other.getClass())
            throw new IllegalArgumentException(
                    "Копирующий конструктор вызван с объектом другого класса");
        this.data = new int[getSize()];
        System.arraycopy(other.data, 0, this.data, 0, getSize());
    }
    
    void write(PacketBodyStream stream) {
        for (int i = 0; i < getSize(); i++) {
            stream.putShort(data[i]);
        }
    }
    
    @Override
    public final boolean equals(Object o) {
        if (this.getClass() != o.getClass()) {
            return false;
        }
        ArrayData other = (ArrayData) o;
        for (int i = 0; i < getSize(); i++) {
            if (data[i] != other.data[i]) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public final int hashCode() {
        int res = 0;
        for (int i = 0; i < getSize(); i++) {
            res *= data[i];
        }
        return res;
    }
    
    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder();
        buf.append("[");
        for (int i = 0; i < getSize() - 1; i++) {
            buf.append(String.format("%d, ", data[i]));
        }
        buf.append(String.format("%d]", data[getSize() - 1]));
        return buf.toString();
    }
    
    protected abstract int getSize();
    
    private int[] data; 

    int[] getData() {
        return data;
    }
    
    public int getValue(int i) {
        return data[i];
    }
    
    public void setValue(int i, int value) {
        data[i] = value;
    }

    public void getData(int i, int value) {
        this.data[i] = value;
    }
}

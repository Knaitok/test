package info.itline.controller;

import java.io.ByteArrayOutputStream;

class PacketBodyStream extends ByteArrayOutputStream {
    public void putPassword(int password) {
        int salt = PasswordHelper.getSalt(password);
        putInt(salt);
        putInt(PasswordHelper.encryptPassword(password, salt));
    } 
    
   void putShort(int value) {
        byte low = (byte) (value & 0xFF);
        byte high = (byte) ((value >> 8) & 0xFF);
        write(low);
        write(high);
    }
    
    void putInt(long value) {
        for (int i = 0; i < 4; i++) {
            byte b = (byte) (value & 0xFF);
            value >>= 8;
            write(b);
        }
    }
    
    void putLong(long value) {
        for (int i = 0; i < 8; i++) {
            byte b = (byte) (value & 0xFF);
            value >>= 8;
            write(b);
        }
    }
}

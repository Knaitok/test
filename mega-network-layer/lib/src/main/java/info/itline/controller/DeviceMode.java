package info.itline.controller;

import java.util.HashMap;

public enum DeviceMode {
    NORMAL      (0),
    GAME        (1),
    THIRD_MODE  (2);
    
    private DeviceMode(long id) {
        this.mId = id;
    }
    
    public long getId() {
        return mId;
    }
    
    private static final HashMap<Long, DeviceMode> mAssoc;
    static {
        mAssoc = new HashMap<>();
        for (DeviceMode m: DeviceMode.values()) {
            mAssoc.put(m.getId(), m);
        }
    }
    
    public static DeviceMode getModeById(long id) {
        return mAssoc.get(id);
    }
    
    private long mId;
}

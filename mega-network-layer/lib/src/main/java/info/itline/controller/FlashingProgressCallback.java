package info.itline.controller;

public interface FlashingProgressCallback {
    void updateProgress(double progress);
}

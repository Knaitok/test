package info.itline.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import java.util.TreeMap;

public enum DeviceType {
    ANY                         (0xFF),
    CURRENCY_RATE_BOARD         (4),
    STADIUM_BOARD               (5),
    QUEUE_BOARD                 (6);
    
    private static final Map<Integer, DeviceType> idMapping = 
            new TreeMap<>();
    
    static {
        for (DeviceType t: DeviceType.values())
            idMapping.put(t.getId(), t);
    }
    
    DeviceType(int id) {
        this.id = id;
    }
    
    void writeToStream(OutputStream s) throws IOException {
        s.write((byte)id);
    }
    
    int getId() {
        return id;
    }
    
    static DeviceType getById(int id) {
        if (idMapping.containsKey(id))
            return idMapping.get(id);
        else
            return null;
    }
    
    private int id;
}
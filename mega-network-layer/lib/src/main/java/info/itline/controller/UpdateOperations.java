package info.itline.controller;

import static info.itline.controller.PasswordType.*;

public enum UpdateOperations {
    CHANGE_NETWORK_SETTINGS(ADMIN),
    CHANGE_DATE_TIME_SETTINGS(USER),
    CHANGE_NVRAM_SETTINGS(USER),
    CHANGE_DISPLAY_DURATION_SETTINGS(ADMIN),
    CHANGE_CREEPING_LINE_SETTINGS(USER),
    CHANGE_SERVER_CONFIG(ADMIN);
    
    private UpdateOperations(PasswordType pt) {
        mPasswordType = pt;
    }
    
    public PasswordType getPasswordType() {
        return mPasswordType;
    }
    
    private PasswordType mPasswordType;
}

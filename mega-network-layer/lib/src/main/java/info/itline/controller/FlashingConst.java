package info.itline.controller;

import java.util.HashMap;
import java.util.Map;

class FlashingConst {

    private FlashingConst() {
    }
    
    enum Command {
        NONE(0), 
        ENTER(1), 
        ABORT(2),
        STATE(3)
        ;
        
        private Command(int id) {
            this.id = id;
        }
        
        static Command getById(int id) {
            return map.get(id);
        }
        
        final int id;
        
        private static final Map<Integer, Command> map = new HashMap<>();
        static {
            for (Command c: Command.values()) {
                map.put(c.id, c);
            }
        }
    }
    
    enum State {
        IDLE(0), 
        ENTER(1), 
        READY(2), 
        WRITE(3), 
        DONE(4), 
        RESULT(5)
        ;
        
        private State(int id) {
            this.id = id;
        }
        
        static State getById(int id) {
            return map.get(id);
        }
        
        final int id;
        
        private static final Map<Integer, State> map = new HashMap<>();
        static {
            for (State c: State.values()) {
                map.put(c.id, c);
            }
        }
    }
    
    enum Result {
        OK(0),
        WRONG_CRC(1);
        
        private Result(int id) {
            this.id = id;
        }
        
        static Result getById(int id) {
            return map.get(id);
        }
        
        final int id;
        
        private static final Map<Integer, Result> map = new HashMap<>();
        static {
            for (Result c: Result.values()) {
                map.put(c.id, c);
            }
        }
    }
    
    enum Response {
        FAILED(0),
        OK(1);
        
        private Response(int id) {
            this.id = id;
        }
        
        static Response getById(int id) {
            return map.get(id);
        }
        
        final int id;
        
        private static final Map<Integer, Response> map = new HashMap<>();
        static {
            for (Response c: Response.values()) {
                map.put(c.id, c);
            }
        }
    }
}
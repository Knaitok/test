package info.itline.controller;

import com.google.common.io.LittleEndianDataInputStream;
import java.io.IOException;

public class DeviceName extends StringData {
    public DeviceName(LittleEndianDataInputStream in) throws IOException {
        super(in);
    }

    public DeviceName(StringData other) {
        super(other);
    }
    
    public DeviceName(String s) {
        super(s);
    }
    
    @Override
    protected int getMaxSize() {
        return SIZE;
    }
    
    public static final int SIZE = 32;
}

package info.itline.controller;

import java.io.IOException;

public class DebugRequestPacket extends RequestPacket {

    public DebugRequestPacket(DeviceSettings settings, boolean enable) {
        super(RequestPacketType.START_DEBUG_LOGGING, settings.getType(), settings.getMACAddress());
        this.enableLogging = enable;
    }

    @Override
    protected PacketBodyStream makeOutputStream() throws IOException {
        PacketBodyStream s = super.makeOutputStream();
        if (UDPClient.IS_MEGA) {
            s.putInt(0);
        }
        s.putInt(enableLogging ? 1 : 0);
        return s;
    }

    public boolean isEnableLogging() {
        return enableLogging;
    }
    
    private final boolean enableLogging;
}

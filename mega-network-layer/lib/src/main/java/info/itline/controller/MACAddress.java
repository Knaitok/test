package info.itline.controller;

import com.google.common.io.LittleEndianDataInputStream;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MACAddress extends HWAddress {
    public MACAddress(byte[] bytes) {
        super(bytes);
    }

    MACAddress(MACAddress other) {
        super(other);
    }
    
    MACAddress(LittleEndianDataInputStream stream) throws IOException {
        super(stream);
    }

    MACAddress(byte[] stream, int offset) {
        super(stream, offset);
    }
    
    public static MACAddress parse(String s) throws IllegalArgumentException {
        Matcher m = MAC_PATTERN.matcher(s);
        IllegalArgumentException exc = 
                new IllegalArgumentException("Строка не является MAC-адресом");
        if (!m.matches() || m.groupCount() != ADDRESS_SIZE) {
            throw exc;
        }
        byte[] buf = new byte[ADDRESS_SIZE];
        for (int i = 0; i < m.groupCount(); i++) {
            buf[i] = (byte) (Integer.parseInt(m.group(i + 1), 16) & 0xFF);
        }
        return new MACAddress(buf);
    }

    @Override
    public final int getAddressSize() {
        return ADDRESS_SIZE;
    }
    
    @Override
    protected String getAddressPartFormat() {
        return "%02x";
    }
    
    @Override
    protected String getAddressSeparatorFormat() {
        return ":";
    }
    
    public static final MACAddress BROADCAST_ADDRESS = new MACAddress(
            new byte[] {-1, -1, -1, -1, -1, -1});
    
    private static final Pattern MAC_PATTERN;
    static {
        String b = "([A-Fa-f0-9]{2})";
        String s = ":";
        MAC_PATTERN = Pattern.compile(
                "^" + b + s + b + s + b + s + b + s + b + s + b + "$");
    }
    
    private static final int ADDRESS_SIZE = 6;
}

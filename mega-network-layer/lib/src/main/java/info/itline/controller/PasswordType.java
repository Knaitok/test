package info.itline.controller;

public enum PasswordType {
    
    ADMIN(0),
    USER(1);
    
    private PasswordType(int id) {
        mId = id;
    }
    
    public int getId() {
        return mId;
    }
    
    private int mId;
}
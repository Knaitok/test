package info.itline.controller;

import com.google.common.io.LittleEndianDataInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

class SearchResponsePacket extends ResponsePacket {
    SearchResponsePacket(LittleEndianDataInputStream body) throws Exceptions.InvalidPacket {
        super(body);
        checkPacketType(ResponsePacketType.SEARCH, logger);
        // checkDeviceType(logger);
        try {
            if (UDPClient.IS_MEGA) {
                body.readInt();
            }
            
            name = new DeviceName(body);
            
            ip = new IPAddress(body);
            mask = new IPAddress(body);
            gateway = new IPAddress(body);
            
            if (getDeviceType() == DeviceType.CURRENCY_RATE_BOARD) {
                displayDurationData = new DisplayDurationData(body);
                deviceMode = DeviceMode.NORMAL;
            }
            else if (getDeviceType() == DeviceType.STADIUM_BOARD) {
                int deviceModeId = body.readInt();
                deviceMode = DeviceMode.getModeById(deviceModeId);
                if (deviceMode == null) {
                    throw new Exceptions.InvalidPacket(
                            "Неправильное значение параметра mode:" + deviceModeId);
                }
            }
            
            if (!UDPClient.IS_STOP_BOARD) {
                if (getDeviceType() != DeviceType.QUEUE_BOARD) {
                    isDisplayOff = body.readUnsignedShort();
                }
                else {
                    isDisplayOff = 0;
                }
                brightness = DisplayBrightness.createFromDeviceValue(body.readUnsignedShort());

                if (getDeviceType() == DeviceType.CURRENCY_RATE_BOARD) {
                    userData = new UserData(body);
                }
            }
            else {
                brightness = DisplayBrightness.createFromDeviceValue(0);
            }
            
            version = body.readUnsignedShort();
            manufacturingDate = body.readUnsignedShort();
        }
        catch (IOException exc) {
            handleIOException(exc, logger);
        }
    }
    
    DeviceName getName() {
        return name;
    }
    
    IPAddress getIp() {
        return ip;
    }

    IPAddress getMask() {
        return mask;
    }

    IPAddress getGateway() {
        return gateway;
    }

    DisplayDurationData getDisplayDurationData() {
        return displayDurationData;
    }
    
    int getIsDisplayOff() {
        return isDisplayOff;
    }

    DisplayBrightness getBrightness() {
        return brightness;
    }

    UserData getUserData() {
        return userData;
    }

    int getVersion() {
        return version;
    }

    int getManufacturingDate() {
        return manufacturingDate;
    }
    
    public DeviceMode getDeviceMode() {
        return deviceMode;
    }
    
    @Override
    public String toString() {
        return super.toString() + String.format(
                "[Name = %s, IP = %s, Mask = %s, Gateway = %s, "
                + "Display OFF = %d, Brightness = %d, Version = %d, Date = %d]", 
                name, ip, mask, gateway, isDisplayOff, brightness.getValue(),
                version, manufacturingDate);
    }
    
    private static final Logger logger = LoggerFactory.getLogger(ResponsePacket.class);
    
    private DeviceName name;
    private IPAddress ip;
    private IPAddress mask;
    private IPAddress gateway;
    private DisplayDurationData displayDurationData;
    private int isDisplayOff;
    private DisplayBrightness brightness;
    private UserData userData;
    private int version;
    private int manufacturingDate;
    private DeviceMode deviceMode;
}

package info.itline.controller;

import com.google.common.io.LittleEndianDataInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

class NetworkSettingsResponsePacket extends ResponsePacket {
    NetworkSettingsResponsePacket(LittleEndianDataInputStream body) throws Exceptions.InvalidPacket {
        super(body);
        checkPacketType(ResponsePacketType.NETWORK_SETTINGS, logger);
        // checkDeviceType(log);
        try {
            name = new DeviceName(body);
            ip = new IPAddress(body);
            mask = new IPAddress(body);
            gateway = new IPAddress(body);            
        }
        catch (IOException exc) {
            handleIOException(exc, logger);
        }
    }
    
    DeviceName getName() {
        return name;
    }

    IPAddress getIp() {
        return ip;
    }

    IPAddress getMask() {
        return mask;
    }

    IPAddress getGateway() {
        return gateway;
    }
    
    private DeviceName name;
    private IPAddress ip;
    private IPAddress mask;
    private IPAddress gateway;
    
    private static final Logger logger = LoggerFactory.getLogger(
            NetworkSettingsResponsePacket.class);
}

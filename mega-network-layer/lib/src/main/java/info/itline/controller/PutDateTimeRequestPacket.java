package info.itline.controller;

import java.io.IOException;

class PutDateTimeRequestPacket extends RequestPacket {
    PutDateTimeRequestPacket(DeviceSettings settings, int password) {
        super(RequestPacketType.PUT_DATE_TIME_SETTINGS, settings.getType(), 
                settings.getMACAddress());
        this.dateTime = settings.getDateTimeAsSeconds();
        this.password = password;
    }
    
    @Override
    protected PacketBodyStream makeOutputStream() throws IOException {
        PacketBodyStream res = super.makeOutputStream();
        res.putInt((int)(dateTime & 0x7FFFFFFF));
        if (UDPClient.IS_STOP_BOARD || getDeviceType() != DeviceType.QUEUE_BOARD) {
            res.putPassword(password);
        }
        return res;
    }
    
    private long dateTime;
    private int password;
}

package info.itline.controller;

import com.google.common.io.LittleEndianDataInputStream;
import java.io.IOException;

public class CreepingLineText extends StringData {
    CreepingLineText(LittleEndianDataInputStream in) throws IOException {
        super(in);
    }

    public CreepingLineText(StringData other) {
        super(other);
    }
    
    public CreepingLineText(String s) {
        super(s);
    }
    
    public CreepingLineText(byte[] data) {
        super(data);
    }
    
    public static CreepingLineText fromInteger(long v) {
        byte[] buf = new byte[4]; 
        for (int i = 0; i < 4; i++) {
            buf[i] = (byte) ((v >> (i * 8)) & 0xFF);
        }
        /* String result = null;
        try {
            result = new String(buf, "ASCII");
        }
        catch (UnsupportedEncodingException e) {
            // ignore
        }
        return new CreepingLineText(result); */
        return new CreepingLineText(buf);
    }
    
    @Override
    protected int getMaxSize() {
        return SIZE;
    }
    
    public static final int SIZE = 1024;
}
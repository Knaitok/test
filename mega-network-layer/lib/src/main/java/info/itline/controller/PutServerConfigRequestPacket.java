package info.itline.controller;

import java.io.IOException;

public class PutServerConfigRequestPacket extends RequestPacket {

    public PutServerConfigRequestPacket(DeviceSettings device, int password) {
        super(RequestPacketType.PUT_SERVER_CONFIG, device.getType(), device.getMACAddress());
        this.serverConfig = new ServerConfig(device.getServerConfig());
        this.password = password;
    }

    @Override
    protected PacketBodyStream makeOutputStream() throws IOException {
        PacketBodyStream result = super.makeOutputStream();
        
        result.putInt(serverConfig.getUpdatingPeriod());
        result.putInt(serverConfig.getWatchdogPeriod());
        result.putInt(serverConfig.getServerPort());
        result.putInt(serverConfig.getControllerId());
        
        serverConfig.getServerName().write(result);
        serverConfig.getPageOnServer().write(result);
        serverConfig.getHostName().write(result);
        
        result.putPassword(password);
        
        return result;
        
    }
    
    private ServerConfig serverConfig;
    private int password;
}

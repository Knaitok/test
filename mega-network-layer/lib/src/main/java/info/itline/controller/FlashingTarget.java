package info.itline.controller;

import java.util.HashMap;
import java.util.Map;

public enum FlashingTarget {
    IMAGE(0, 1024 * 128), FONT(1, 1024 * 128), CONFIG(2, 1024 * 64);

    private FlashingTarget(int id, int size) {
        this.id = id;
        this.targetSize = size;
    }

    public int getSectionSize() {
        return targetSize;
    }
    
    static FlashingTarget getById(int id) {
        return map.get(id);
    }
    
    final int id;
    final int targetSize;
    
    private static final Map<Integer, FlashingTarget> map = new HashMap<>();
    static {
        for (FlashingTarget c : FlashingTarget.values()) {
            map.put(c.id, c);
        }
    }
}

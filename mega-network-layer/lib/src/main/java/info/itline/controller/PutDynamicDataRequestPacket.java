package info.itline.controller;

import java.io.IOException;

class PutDynamicDataRequestPacket extends RequestPacket {
    public PutDynamicDataRequestPacket(DeviceSettings settings, int password) {
        super(RequestPacketType.PUT_DYNAMIC_DATA, settings.getType(), 
                settings.getMACAddress());
        this.displayDurationData = new DisplayDurationData(settings.getDisplayDurationData());
        this.password = password;
    }
    
    @Override
    protected PacketBodyStream makeOutputStream() throws IOException {
        PacketBodyStream res = super.makeOutputStream();
        displayDurationData.write(res);
        if (getDeviceType() != DeviceType.QUEUE_BOARD) {
            res.putPassword(password);
        }
        return res;
    }
    
    private DisplayDurationData displayDurationData;
    private int password;
}

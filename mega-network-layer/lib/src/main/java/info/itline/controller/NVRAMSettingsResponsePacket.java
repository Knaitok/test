package info.itline.controller;

import com.google.common.io.LittleEndianDataInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

class NVRAMSettingsResponsePacket extends ResponsePacket {
    NVRAMSettingsResponsePacket(LittleEndianDataInputStream body) throws Exceptions.InvalidPacket {
        super(body);
        checkPacketType(ResponsePacketType.NVRAM_DATA, logger);
        // checkDeviceType(logger);
        try {
            if (!UDPClient.IS_STOP_BOARD) {
                isDisplayOff = body.readShort() == 0 ? false : true;
            }
            brightness = DisplayBrightness.createFromDeviceValue(
                    UDPClient.IS_STOP_BOARD ? body.readInt() : body.readShort());
            if (getDeviceType() == DeviceType.CURRENCY_RATE_BOARD) {
                userData = new UserData(body);
            }
        }
        catch (IOException exc) {
            handleIOException(exc, logger);
        }
    }
    
    boolean getIsDisplayOff() {
        return isDisplayOff;
    }

    DisplayBrightness getBrightness() {
        return brightness;
    }

    UserData getUserData() {
        return userData;
    }
    
    private boolean isDisplayOff;
    private DisplayBrightness brightness;
    private UserData userData;
    
    private static final Logger logger = LoggerFactory.getLogger(
            NVRAMSettingsResponsePacket.class);
}

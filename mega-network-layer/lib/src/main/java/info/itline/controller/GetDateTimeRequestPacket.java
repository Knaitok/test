package info.itline.controller;

import java.io.IOException;

class GetDateTimeRequestPacket extends RequestPacket {
    GetDateTimeRequestPacket(DeviceSettings settings) {
        super(RequestPacketType.GET_DATE_TIME_SETTINGS, settings.getType(),
                settings.getMACAddress());
    }

    @Override
    protected PacketBodyStream makeOutputStream() throws IOException {
        PacketBodyStream s = super.makeOutputStream();
        if (UDPClient.IS_MEGA) {
            s.putInt(0);
        }
        return s;
    }
}

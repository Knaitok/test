package info.itline.controller;

import com.google.common.io.LittleEndianDataInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Arrays;

public abstract class StringData {
    public StringData(String s) {
        int maxSize = getMaxSize();
        if (s.length() > maxSize- 1)
            throw new IllegalArgumentException("Строка слишком длинная");
        stringRep = s;
        data = new byte[maxSize];
        Arrays.fill(data, (byte) 0);
        byte[] stringBytes = null;
        try {
            stringBytes = s.getBytes(ENCODING_NAME);
        }
        catch (UnsupportedEncodingException exc) {
            throw new RuntimeException("Кодирока не поддерживается, нужно проверить "
                    + "isEncodingSupported перед использованием этого класса");
        }
        System.arraycopy(stringBytes, 0, data, 0, stringBytes.length); 
    }
    
    public StringData(byte[] data) {
        if (data.length > getMaxSize()) {
            throw new RuntimeException("Data is too large");
        }
        this.data = new byte[getMaxSize()];
        System.arraycopy(data, 0, this.data, 0, data.length);
        this.stringRep = "Binary data";
    }
    
    public static boolean isEncodingSupported() {
        return Charset.isSupported(ENCODING_NAME);
    }
    
    StringData(LittleEndianDataInputStream in) throws IOException {
        int maxSize = getMaxSize();
        data = new byte[maxSize];
        in.read(data);
        data[maxSize - 1] = 0;
        int length = maxSize - 1;
        for (int i = 0; i < maxSize; i++) {
            if (data[i] == 0) {
                length = i;
                break;
            }
        }
        stringRep = new String(data, 0, length, "ASCII");
    }
    
    StringData(StringData other) {
        if (this.getClass() != other.getClass())
            throw new IllegalArgumentException(
                    "Копирующий конструктор вызван с объектом другого класса");
        int maxSize = getMaxSize();
        this.data = new byte[maxSize];
        System.arraycopy(other.data, 0, this.data, 0, maxSize);
        this.stringRep = other.stringRep;
    }
    
    void write(PacketBodyStream stream) throws IOException {
        stream.write(data);
    }
    
    @Override
    public final boolean equals(Object o) {
        if (this.getClass() != o.getClass()) {
            return false;
        }
        return this.stringRep.equals(((StringData) o).stringRep);
    }
    
    @Override
    public final int hashCode() {
        return stringRep.hashCode();
    }
    
    protected abstract int getMaxSize();
    
    @Override
    public String toString() {
        return stringRep;
    }
    
    /* public void setValue(String value) throws IllegalArgumentException {
        byte[] buf = value.getBytes(Charset.forName("ASCII"));
        if (buf.length > getMaxSize() - 1)
            throw new IllegalArgumentException("Value too long");
        Arrays.fill(data, 0, getMaxSize() - 1, (byte) 0);
        System.arraycopy(buf, 0, data, 0, buf.length);
    } */
    
    private byte[] data;
    private String stringRep;
    
    private static final String ENCODING_NAME = "CP1251";
}

package info.itline.controller;

import java.io.IOException;

public class PutCreepingLineSettingsRequestPacket extends RequestPacket {

    public PutCreepingLineSettingsRequestPacket(DeviceSettings settings, int password) {
        super(RequestPacketType.PUT_CREEPING_LINE_SETTINGS, settings.getType(), 
                settings.getMACAddress());
        this.password = password;
        this.mode = settings.getDeviceMode();
        this.number = settings.getCreepingLineNumber();
        this.param = settings.getCreepingLineParam();
        this.creepingLineText = new CreepingLineText(settings.getCreepingLineText());
    }
    
    @Override
    protected PacketBodyStream makeOutputStream() throws IOException {
        PacketBodyStream result = super.makeOutputStream();
        if (getDeviceType() != DeviceType.QUEUE_BOARD) {
            result.putPassword(password);
        }
        result.putInt(mode.getId());
        result.putInt(number);
        result.putInt(param);
        creepingLineText.write(result);
        return result;
    }
    
    private int password;
    private DeviceMode mode;
    private long number;
    private long param;
    private CreepingLineText creepingLineText;
}

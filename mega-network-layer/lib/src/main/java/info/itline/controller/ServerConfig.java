package info.itline.controller;

public class ServerConfig {

    public ServerConfig() {
    }

    public static ServerConfig createFakeConfig() {
        ServerConfig c = new ServerConfig();
        c.serverName = new ServerNameString("google.com");
        c.hostName = new HostNameString("google.com");
        c.pageOnServer = new PageOnServerString("api/data");
        c.controllerId = 1;
        c.serverPort = 8080;
        c.updatingPeriod = 10;
        c.watchdogPeriod = 0;
        return c;
    }
    
    public ServerConfig(ServerNameString serverName, HostNameString hostName, 
            PageOnServerString pageOnServer, int controllerId, int serverPort, 
            int updatingPeriod, int watchdogPeriod) {
        this.serverName = serverName;
        this.hostName = hostName;
        this.pageOnServer = pageOnServer;
        this.controllerId = controllerId;
        this.serverPort = serverPort;
        this.updatingPeriod = updatingPeriod;
        this.watchdogPeriod = watchdogPeriod;
    }
    
    public ServerConfig(ServerConfig o) {
        this.serverName = new ServerNameString(o.serverName);
        this.hostName = new HostNameString(o.hostName);
        this.pageOnServer = new PageOnServerString(o.pageOnServer);
        this.controllerId = o.controllerId;
        this.serverPort = o.serverPort;
        this.updatingPeriod = o.updatingPeriod;
        this.watchdogPeriod = o.watchdogPeriod;
    }

    public ServerNameString getServerName() {
        return serverName;
    }

    public void setServerName(ServerNameString serverName) {
        this.serverName = serverName;
    }

    public HostNameString getHostName() {
        return hostName;
    }

    public void setHostName(HostNameString hostName) {
        this.hostName = hostName;
    }

    public PageOnServerString getPageOnServer() {
        return pageOnServer;
    }

    public void setPageOnServer(PageOnServerString pageOnServer) {
        this.pageOnServer = pageOnServer;
    }

    public int getControllerId() {
        return controllerId;
    }

    public void setControllerId(int controllerId) {
        this.controllerId = controllerId;
    }

    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public int getUpdatingPeriod() {
        return updatingPeriod;
    }

    public void setUpdatingPeriod(int updatingPeriod) {
        this.updatingPeriod = updatingPeriod;
    }

    public int getWatchdogPeriod() {
        return watchdogPeriod;
    }

    public void setWatchdogPeriod(int watchdogPeriod) {
        this.watchdogPeriod = watchdogPeriod;
    }
    
    private ServerNameString serverName;
    private HostNameString hostName;
    private PageOnServerString pageOnServer;
    private int controllerId, serverPort, updatingPeriod, watchdogPeriod;
}

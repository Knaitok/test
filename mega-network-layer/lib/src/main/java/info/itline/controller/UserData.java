package info.itline.controller;

import com.google.common.io.LittleEndianDataInputStream;
import java.io.IOException;

public class UserData extends ArrayData {
    public UserData(int[] data) {
        super(data);
    }

    UserData(LittleEndianDataInputStream in)
            throws IOException {
        super(new int[SIZE]);
        for (int i = 0; i < SIZE; i++) {
            setValue(i, IS_EXTENDED_BOARD ? in.readInt() : in.readShort());
        }
    }
    
    UserData(UserData other) {
       super(other);
    }

    @Override
    void write(PacketBodyStream stream) {
        if (IS_EXTENDED_BOARD) {
            for (int i = 0; i < SIZE; i++) {
                stream.putInt(getValue(i));
            }
        }
        else {
            super.write(stream);
        }
    }
    
    @Override
    protected int getSize() {
        return SIZE;
    }
    
    public static final boolean IS_EXTENDED_BOARD = true;
    public static final int SIZE = IS_EXTENDED_BOARD ? 64 : 32;
}

package info.itline.controller;

import com.google.common.io.LittleEndianDataInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.util.LinkedList;
import java.util.List;

public class UDPClient {

    private static final int SERVER_SOCKET_PORT = 5001;
    private static final int LOG_PACKET_SIZE = 128;
    private static final int LOG_TIMEOUT = 500;
    private static final long LOG_DELAY_AFTER_INTERRUPT = 10_000;
    private static final String LOG_FILE = "flashing.logger";
    private static final int MAX_PACKET_SIZE = 2048;
    private static final int SOCKET_TIMEOUT = 500;
    private static IPAddress broadcastAddress;
    private static UDPClient instance;
    private static final Logger logger = LoggerFactory.getLogger(UDPClient.class);
    private static DatagramSocket socket;
    
    public Thread loggerThread;
    
    private UDPClient() throws UnknownHostException, SocketException {
        broadcastAddress = new IPAddress(new byte[] {-1, -1, -1, -1});
        socket = new DatagramSocket();
        socket.setSoTimeout(SOCKET_TIMEOUT);
    }
    
    public boolean startLogging(DeviceSettings settings, boolean broadcast) {
        if (loggerThread != null && loggerThread.isAlive()) {
            return false;
        }
        loggerThread = new Thread(new LoggerRunnable(settings, broadcast));
        loggerThread.setDaemon(true);
        loggerThread.start();
        logger.info("Log thread started");
        return true;
    } 
    
    public void stopLogging() {
        if (loggerThread != null && loggerThread.isAlive()) {
            loggerThread.interrupt();
        } 
    }
    
    private static class LoggerRunnable implements Runnable {

        public LoggerRunnable(final DeviceSettings s, boolean broadcast) {
            this.ip = broadcast ? broadcastAddress : s.getIp();
            this.req = new DebugRequestPacket(s, true);
        }
        
        @Override
        public void run() {
            DatagramSocket s = null;
            DatagramPacket p = new DatagramPacket(
                    new byte[LOG_PACKET_SIZE], LOG_PACKET_SIZE);
            PrintStream logStream = null;
            Charset logCharset = null;

            try {
                logCharset = Charset.forName("windows-1251");
                logStream = new PrintStream(LOG_FILE);
                s = new DatagramSocket();
                s.setSoTimeout(LOG_TIMEOUT);
                send(ip, req);
            }
            catch (IOException | UnsupportedCharsetException e) {
                logger.warn("Unable to start controller logging");
                if (s != null) {
                    s.close();
                }
                if (logStream != null) {
                    logStream.close();
                }
            }

            if (s == null || logStream == null) {
                return;
            }

            long lastLogEnableTimestamp = System.currentTimeMillis();
            final long logEnablePeriod = 10_000;
            
            long interruptTimestamp = 0;
            boolean alreadyInterrupted = false;
            while (true) {
                try {
                    if (System.currentTimeMillis() - lastLogEnableTimestamp > logEnablePeriod) {
                        lastLogEnableTimestamp = System.currentTimeMillis();
                        send(ip, req);
//                        logger.info("Debug on");
                    }
                    
                    s.receive(p);
                    logStream.append(new String(p.getData(), logCharset));
                }
                catch (SocketTimeoutException ignore) {
//                    logger.info("Log socket timeout");
                }
                catch (IOException e) {
                    logger.warn("Unable to receive logger chunk");
                }

                if (Thread.currentThread().isInterrupted() && !alreadyInterrupted) {
                    alreadyInterrupted = true;
                    interruptTimestamp = System.currentTimeMillis();
                }

                if (alreadyInterrupted && 
                        System.currentTimeMillis() - interruptTimestamp > 
                                LOG_DELAY_AFTER_INTERRUPT) {
                    break;
                }
            }

            logStream.flush();
            logStream.close();
            s.close();

            logger.info("Terminating logger thread");
        }
        
        private final IPAddress ip;
        private final DebugRequestPacket req;
    }
    
    public void flashDevice(final DeviceSettings settings, FlashingTarget target, byte[] data, 
            FlashingProgressCallback cb, boolean useBroadcastAddress) throws Exceptions.OperationFailed {
        assert(data.length <= target.targetSize);
        
        final IPAddress ip = useBroadcastAddress ? broadcastAddress : settings.getIp();
        
        byte[] buf = new byte[target.targetSize];
        System.arraycopy(data, 0, buf, 0, data.length);
        for (int i = data.length; i < target.targetSize; i++) {
            buf[i] = (byte) 0xFF;
        }
        
        final int bs = FlashingDataRequestPacket.DATA_BLOCK_SIZE;
        final int blockCount = target.targetSize / bs;
        logger.info("Block count = {}", blockCount);
        
        double progress = 0;
        final double progressStep = 1.0 / blockCount;
        
        try {
            socket.setSoTimeout(2000);
            FlashingCommandRequestPacket abortCmd = new FlashingCommandRequestPacket(settings, FlashingConst.Command.ABORT, target);
            send(ip, abortCmd);
            receiveFlashingResponsePacket(settings, null);
            
            int addr = -1;
            
            FlashingCommandRequestPacket cmd = new FlashingCommandRequestPacket(settings, FlashingConst.Command.ENTER, target);
            send(ip, cmd);
            FlashingResponsePacket resp = receiveFlashingResponsePacket(settings, null);
            for (int i = 0; i < 20; i++) {
                if (resp.getState() == FlashingConst.State.READY) {
                    addr = resp.getMemoryAddress();
                    break;
                }
                else {
                    logger.info("Device is not ready yet, state = {}", resp.getState());
                    try {
                        Thread.sleep(500);
                    }
                    catch (InterruptedException ignore) {
                        
                    }
                    resp = getFlashingState(settings, useBroadcastAddress);
                }
            }
            try {
                for (int i = 0; i < 3; i++) {
                    receiveFlashingResponsePacket(settings, null);
                }
            }
            catch (Exception ignore) {
                
            }
            logger.info("Device is ready for flashing, start addr = {}", addr);
            if (addr != 0) {
                throw new Exceptions.OperationFailed(FLASH_DEVICE, "Внутренняя ошибка, начальный адрес не равен нулю");
            }
            
            for (int i = 0; i < blockCount; i++) {
                FlashingDataRequestPacket req = new FlashingDataRequestPacket(settings, target, addr, buf, addr);
                send(ip, req);
                try {
                    Thread.sleep(25);
                }
                catch (InterruptedException ignore) {

                }
                if (i == blockCount - 1) {
                    receiveFlashingResponsePacket(settings, FlashingConst.State.DONE);
                }
                else {
                    int newAddr = receiveFlashingReadyResponse(settings);
                    logger.info("Block = {}, Next address = {}", i, newAddr);
                    if (newAddr != addr + bs) {
                        throw new Exceptions.OperationFailed(FLASH_DEVICE, "Рассинхронизация в процессе прошивки");
                    }
                    addr = newAddr;
                }
                progress += progressStep;
                cb.updateProgress(progress);
            }
            
            FlashingConst.Result r = receiveFlashingDoneResponse(settings);
            if (r != FlashingConst.Result.OK) {
                throw new Exceptions.OperationFailed(FLASH_DEVICE, "Контроллер вернул сообщение об ошибке " + r);
            }
        }
        catch (IOException | Exceptions.InvalidPacket e) {
            throw new Exceptions.OperationFailed(FLASH_DEVICE, e.getMessage());
        }
//        finally {
//            try {
//                socket.setSoTimeout(SOCKET_TIMEOUT);
//            }
//            catch (SocketException ignore) {
//                
//            }
//        }
    }
    
    private int receiveFlashingReadyResponse(DeviceSettings s) 
            throws IOException, Exceptions.InvalidPacket {
        return receiveFlashingResponsePacket(s, FlashingConst.State.READY).getMemoryAddress();
    }
    
    private FlashingConst.Result receiveFlashingDoneResponse(DeviceSettings s) 
            throws IOException, Exceptions.InvalidPacket {
        return receiveFlashingResponsePacket(s, FlashingConst.State.RESULT).getResult();
    }
    
    private FlashingResponsePacket getFlashingState(DeviceSettings settings, boolean useBroadcastRequests)
            throws IOException, Exceptions.InvalidPacket {
        FlashingCommandRequestPacket req = new FlashingCommandRequestPacket(settings, FlashingConst.Command.STATE, FlashingTarget.IMAGE);
        send(useBroadcastRequests ? broadcastAddress : settings.getIp(), req);
        return receiveFlashingResponsePacket(settings, null);
    }
    
    private FlashingResponsePacket receiveFlashingResponsePacket(DeviceSettings s, 
            FlashingConst.State state) throws IOException, Exceptions.InvalidPacket {
        DatagramPacket p = new DatagramPacket(new byte[MAX_PACKET_SIZE], MAX_PACKET_SIZE);
        socket.receive(p);
        FlashingResponsePacket frp = new FlashingResponsePacket(makeStreamFromDatagram(p));
        if (!frp.getMACAddress().equals(s.getMACAddress())) {
            throw new Exceptions.InvalidPacket(
                    "Ожидался ответ от устройства " + s.getMACAddress() + ", a получен от " + frp.getMACAddress());
        }
        if (state != null && frp.getState() != state) {
            throw new Exceptions.InvalidPacket("Неправильное состояние устройства: " + frp.getState());
        }
        return frp;
    }
    
    public List<DeviceSettings> searchForDevices(DeviceType deviceType) throws Exceptions.OperationFailed {
        SearchRequestPacket packet = new SearchRequestPacket(deviceType);
        List<DatagramPacket> responses = communicateWithTimeout(
                SEARCH_DESCRIPTION, broadcastAddress, packet, false);
        
        List<DeviceSettings> result = new LinkedList<>();
        for (DatagramPacket r: responses) {
            try {
                SearchResponsePacket srp = new SearchResponsePacket(
                        makeStreamFromDatagram(r));
                if (srp.getDeviceType() != deviceType) {
                    logger.error("Ответ от устройства с другим типом {}", srp.getDeviceType());
                } else {
                    DeviceSettings dev = new DeviceSettings(srp);
                    result.add(dev);
                    logger.info("Найдено устройство: {}", dev);
                }
            }
            catch (Exceptions.InvalidPacket exc) {
                logger.info("Получен некорректный пакет при поиске устройств: {}", exc.getMessage());
            }
        }
        
        for (DeviceSettings settings: result) {
            try {
                synchronizeDateTimeSettings(settings, true);
                logger.info("Время синхронизировано с уcтройством {}: {}",
                        settings.getMACAddress(), settings.formatDateTime());
                if (CAN_UPDATE_FROM_SERVER) {
                    synchronizeServerConfig(settings, true);
                    logger.info("Конфигурация сервера сихронизирована с уcтройством {}",
                        settings.getMACAddress());
                }
                if (IS_STOP_BOARD) {
                    synchronizeStopServerConfig(settings, true);
                    logger.info("Конфигурация сервера сихронизирована с уcтройством {}",
                        settings.getMACAddress());
                }
            }
            catch (Exceptions.OperationFailed exc) {
                logger.error("Не удалось получить дату и время с утройства {}: {}",
                        settings.getMACAddress(), exc);
            }
        }
        
//        result.add(DeviceSettings.createFakeDeviceSettings());
//        // TESTING
//        DeviceSettings s0 = result.get(0);
//        DeviceSettings s1 = new DeviceSettings(s0);
//        s1.setMacAddress(new MACAddress(new byte[] {0, 1, 2, 3, 4, 5}));
//        result.add(s1);
//        // TESTING
        return result;
    }
    
    private static interface SynchronizeAction {
        public void perform(DatagramPacket dg) 
                throws Exceptions.InvalidPacket;
    }
    
    private void performSynchronyzeAction(IPAddress ip, MACAddress mac, RequestPacket request, 
            SynchronizeAction action, String operationDescription, boolean broadcastRequest)
            throws Exceptions.OperationFailed {
        List<DatagramPacket> responses = communicateWithTimeout(
                operationDescription, broadcastRequest ? broadcastAddress : ip, request, false);
        if (responses.isEmpty()) {
            handleTimeoutError(mac, operationDescription);
        }
        boolean actionPerformed = false;
        for (DatagramPacket dg: responses) {
            try {
                ResponsePacket p = new ResponsePacket(makeStreamFromDatagram(dg));
                if (isExpectedDevice(request, p, ip, mac, dg.getAddress(), p.getMACAddress(), !broadcastRequest)) {
                    action.perform(dg);
                    actionPerformed = true;
                }
                else {
                    logger.error("Получен пакет от устройства, запрос к которому не выполнялся: {}",
                            p.getMACAddress());
                }
            }
            catch (Exceptions.InvalidPacket exc) {
                logger.error("Некорректный пакет от {}: {}", dg.getAddress(), exc);
            }
        }
        if (!actionPerformed) {
            handleTimeoutError(mac, operationDescription);
        }
    }
    
    public DeviceSettings synchronizeDeviceSettings(DeviceType type, IPAddress ip, 
            MACAddress mac, boolean broadcastRequest) 
            throws Exceptions.OperationFailed {
        final DeviceSettings[] settings = new DeviceSettings[1];
        SynchronizeAction action = new SynchronizeAction() {
            @Override
            public void perform(DatagramPacket dg) 
                    throws Exceptions.InvalidPacket {
                SearchResponsePacket packet = new SearchResponsePacket(
                        makeStreamFromDatagram(dg));
                logger.info("Настройки синхронизированы с {}", packet.getIp());
                settings[0] = new DeviceSettings(packet);
            }
        };
        
        DeviceSettings result;
        try {
            performSynchronyzeAction(ip, mac, new GetInfoRequestPacket(type, mac), 
                    action, SYNCHRONIZE_DEVICE_SETTINGS_DESCRIPTION, broadcastRequest);
            result = settings[0];
        }
        catch (Exceptions.TimedOut exc) {
            logger.info("Устройcтво не найдено: {}", ip);
            result = null;
        }
        
        if (result != null) {
            synchronizeDateTimeSettings(result, broadcastRequest);
            if (CAN_UPDATE_FROM_SERVER) {
                synchronizeServerConfig(result, broadcastRequest);
                logger.info("Конфигурация сервера сихронизирована с уcтройством {}",
                    result.getMACAddress());
            }
        }
        return result;
    }
    
    private void synchronizeDateTimeSettings(final DeviceSettings settings, boolean broadcastRequest) 
            throws Exceptions.OperationFailed {
        SynchronizeAction action = new SynchronizeAction() {
            @Override
            public void perform(DatagramPacket dg) 
                    throws Exceptions.InvalidPacket {
                DateTimeResponsePacket p = new DateTimeResponsePacket(
                        makeStreamFromDatagram(dg));
                logger.info("Получено значение даты для {}",
                            settings.getMACAddress());
                settings.setDateTime(p.getDateTime());
            }
        };
        performSynchronyzeAction(settings.getIp(), settings.getMACAddress(), 
                new GetDateTimeRequestPacket(settings), action, 
                SYNCHRONYZE_DATETIME_SETTINGS_DESCRIPTION, broadcastRequest);
    }
    
    private void synchronizeServerConfig(final DeviceSettings settings, boolean broadcastRequest)
            throws Exceptions.OperationFailed {
        SynchronizeAction action = new SynchronizeAction() {
            @Override
            public void perform(DatagramPacket dg) 
                    throws Exceptions.InvalidPacket {
                ServerConfigResponsePacket p = new ServerConfigResponsePacket(
                        makeStreamFromDatagram(dg));
                logger.info("Получена конфигурация сервера для {}",
                            settings.getMACAddress());
                System.err.println("Server config = " + p.getServerConfig());
                settings.setServerConfig(p.getServerConfig());
            }
        };
        performSynchronyzeAction(settings.getIp(), settings.getMACAddress(), 
                new GetServerConfigRequestPacket(settings), action, 
                SYNCHRONYZE_SERVER_CONFIG_DESCRIPTION, broadcastRequest);
    }
    
    private void synchronizeStopServerConfig(final DeviceSettings settings, boolean broadcastRequest)
            throws Exceptions.OperationFailed {
        SynchronizeAction action = new SynchronizeAction() {
            @Override
            public void perform(DatagramPacket dg) 
                    throws Exceptions.InvalidPacket {
                StopServerConfigResponsePacket p = new StopServerConfigResponsePacket(
                        makeStreamFromDatagram(dg));
                logger.info("Получена конфигурация сервера для {}",
                            settings.getMACAddress());
                System.err.println("Server config = " + p.getStopServerConfig());
                settings.setStopServerConfig(p.getStopServerConfig());
            }
        };
        performSynchronyzeAction(settings.getIp(), settings.getMACAddress(), 
                new GetStopServerConfigRequestPacket(settings), action, 
                SYNCHRONYZE_SERVER_CONFIG_DESCRIPTION, broadcastRequest);
    }
    
    private static interface UpdateAction {
        public void perform(DatagramPacket p, DeviceSettings settings) 
                throws Exceptions.OperationFailed, Exceptions.InvalidPacket;
    }
    
    private void performUpdateAction(DeviceSettings oldSettings, DeviceSettings newSettings,
            RequestPacket request, ResponsePacketType responsePacketType, 
            UpdateAction action, String operationDescription, boolean broadcastRequest, boolean singleReply)
            throws Exceptions.OperationFailed {
        List<DatagramPacket> packets = communicateWithTimeout(
                operationDescription, broadcastRequest ? broadcastAddress : oldSettings.getIp(), request, singleReply);
        if (packets.isEmpty()) {
            handleTimeoutError(oldSettings.getMACAddress(), operationDescription);
        }
        boolean actionPerformed = false;
        for (DatagramPacket dg: packets) {
            try {
                ResponsePacket rp = new ResponsePacket(makeStreamFromDatagram(dg));
                InetAddress ip = dg.getAddress();
                MACAddress mac = rp.getMACAddress();
                if (rp.getResponsePacketType() == ResponsePacketType.BAD_PASSWORD &&
                        isExpectedDevice(request, rp, oldSettings.getIp(), oldSettings.getMACAddress(), ip, mac, !broadcastRequest)) {
                    logger.error("Неверный пароль для {}", mac);
                    throw new Exceptions.WrongPassword(operationDescription);
                }
                else if (rp.getResponsePacketType() == responsePacketType &&
                        isExpectedDevice(request, rp, newSettings.getIp(), newSettings.getMACAddress(), ip, mac, !broadcastRequest)) {
                   action.perform(dg, newSettings);
                   actionPerformed = true;
                }
                else {
                    logger.error("Неверный тип пакета {} от {}", rp.getResponsePacketType(),
                            rp.getMACAddress());
                }
            }
            catch (Exceptions.InvalidPacket exc) {
                logger.error("Получен некорректный пакет {} при во время операции '{}': {}",
                        dg.getAddress(), operationDescription, exc);
            }
        }
        if (!actionPerformed) {
            handleTimeoutError(oldSettings.getMACAddress(), operationDescription);
        }
    }
    
    public void updateDeviceNetworkSettings(DeviceSettings oldSettings, 
            DeviceSettings newSettings, int password, boolean broadcastRequest) 
            throws Exceptions.OperationFailed {
        UpdateAction action = new UpdateAction() {
            @Override public void perform(DatagramPacket dg, DeviceSettings settings) 
                    throws Exceptions.OperationFailed, Exceptions.InvalidPacket {
                NetworkSettingsResponsePacket nsrp = new NetworkSettingsResponsePacket(
                            makeStreamFromDatagram(dg));
                    boolean correctSettings = nsrp.getIp().equals(settings.getIp()) &&
                            nsrp.getGateway().equals(settings.getGateway()) &&
                            nsrp.getMask().equals(settings.getMask()) &&
                            nsrp.getName().equals(settings.getName());
                    checkSettingsCorrectness(correctSettings, settings.getMACAddress(), 
                            UPDATE_NETWORK_SETTINGS_DESCRIPTION);
            }
        };
        performUpdateAction(oldSettings, newSettings, new PutNetworkSettingsRequestPacket(newSettings, password), 
                ResponsePacketType.NETWORK_SETTINGS, action, UPDATE_NETWORK_SETTINGS_DESCRIPTION, broadcastRequest, false);
    }
    
    public void updateNVRAMData(DeviceSettings settings, int password, boolean broadcastRequest) 
            throws Exceptions.OperationFailed {
       UpdateAction action = new UpdateAction() {
            @Override public void perform(DatagramPacket dg, DeviceSettings settings) 
                    throws Exceptions.OperationFailed, Exceptions.InvalidPacket {
                NVRAMSettingsResponsePacket nvrp = new NVRAMSettingsResponsePacket(
                        makeStreamFromDatagram(dg));
                boolean isCorrectResponse = /* nvrp.getBrightness().equals(settings.getBrigtness()) && */
                        /* nvrp.getIsDisplayOff() == settings.getIsDisplayOff() && */
                        (settings.getType() == DeviceType.CURRENCY_RATE_BOARD ? 
                                nvrp.getUserData().equals(settings.getUserData()) : true);
                checkSettingsCorrectness(isCorrectResponse, settings.getMACAddress(), 
                        UPDATE_NVRAM_SETTINGS_DESCRIPTION);
            }
        };
        performUpdateAction(settings, settings, new PutNVRAMSettingsRequestPacket(settings, password), 
                ResponsePacketType.NVRAM_DATA, action, UPDATE_NVRAM_SETTINGS_DESCRIPTION, broadcastRequest, false);
    }
    
    public void updateDateTimeSettings(DeviceSettings settings, int password, boolean broadcastRequest) 
            throws Exceptions.OperationFailed {
        UpdateAction action = new UpdateAction() {
            @Override public void perform(DatagramPacket dg, DeviceSettings settings) 
                    throws Exceptions.OperationFailed, Exceptions.InvalidPacket {
                logger.info("Дата и время установлены на {}", settings.getMACAddress());
            }
        };
        performUpdateAction(settings, settings, new PutDateTimeRequestPacket(settings, password), 
                ResponsePacketType.DATE_TIME_SETTINGS, action, UPDATE_DATE_TIME_SETTINGS_DESCRIPTION, broadcastRequest, false);
    }
    
    public void updateDynamicData(DeviceSettings settings, int password, boolean broadcastRequest) 
            throws Exceptions.OperationFailed {
        UpdateAction action = new UpdateAction() {
            @Override public void perform(DatagramPacket dg, DeviceSettings settings) 
                    throws Exceptions.OperationFailed, Exceptions.InvalidPacket {
                DynamicDataResponsePacket ddrp = new DynamicDataResponsePacket(
                            makeStreamFromDatagram(dg));
                checkSettingsCorrectness(
                        settings.getType() == DeviceType.CURRENCY_RATE_BOARD ? 
                                settings.getDisplayDurationData().equals(ddrp.getDisplayDurationData()) : true, 
                        settings.getMACAddress(), UPDATE_DYNAMIC_DATA_DESCRIPTION);
                logger.info("Время отображения установлено на {}", settings.getMACAddress());
            }
        };
        performUpdateAction(settings, settings, new PutDynamicDataRequestPacket(settings, password), 
                ResponsePacketType.DYNAMIC_DATA, action, UPDATE_DYNAMIC_DATA_DESCRIPTION, broadcastRequest, false);
    }
    
    public void updateCreepingLineSettings(DeviceSettings settings, int password, boolean broadcastRequest, boolean singleReply) 
            throws Exceptions.OperationFailed {
        UpdateAction action = new UpdateAction() {
            @Override public void perform(DatagramPacket dg, DeviceSettings settings) 
                    throws Exceptions.OperationFailed, Exceptions.InvalidPacket {
                CreepingLineSettingsResponsePacket dmrp = new CreepingLineSettingsResponsePacket(
                        makeStreamFromDatagram(dg));
                settings.setCreepingLineStatus(dmrp.getStatus());
                logger.info("Режим работы обновлен на {}", settings.getMACAddress());
            }
        };
        performUpdateAction(settings, settings, new PutCreepingLineSettingsRequestPacket(settings, password), 
                ResponsePacketType.CREEPING_LINE_SETTINGS, action, UPDATE_CREEPING_LINE_SETTINGS_DESCRIPTION, broadcastRequest, singleReply);
    }
    
    public void updateCreepingLineSettings(DeviceSettings settings, int password, boolean broadcastRequest) 
            throws Exceptions.OperationFailed {
        UpdateAction action = new UpdateAction() {
            @Override public void perform(DatagramPacket dg, DeviceSettings settings) 
                    throws Exceptions.OperationFailed, Exceptions.InvalidPacket {
                CreepingLineSettingsResponsePacket dmrp = new CreepingLineSettingsResponsePacket(
                        makeStreamFromDatagram(dg));
                settings.setCreepingLineStatus(dmrp.getStatus());
                logger.info("Режим работы обновлен на {}", settings.getMACAddress());
            }
        };
        performUpdateAction(settings, settings, new PutCreepingLineSettingsRequestPacket(settings, password), 
                ResponsePacketType.CREEPING_LINE_SETTINGS, action, UPDATE_CREEPING_LINE_SETTINGS_DESCRIPTION, broadcastRequest, false);
    }
    
    public void updateServerConfig(DeviceSettings oldSettings, 
            DeviceSettings newSettings, int password, boolean broadcastRequest) 
            throws Exceptions.OperationFailed {
        UpdateAction action = new UpdateAction() {

            @Override
            public void perform(DatagramPacket p, DeviceSettings settings) throws Exceptions.OperationFailed, Exceptions.InvalidPacket {
                ServerConfigResponsePacket scrp = new ServerConfigResponsePacket(
                        makeStreamFromDatagram(p));
                ServerConfig res = scrp.getServerConfig();
                ServerConfig req = settings.getServerConfig();
                 boolean correctSettings = 
                            res.getControllerId() == req.getControllerId() &&
                            res.getServerPort()== req.getServerPort() &&
                            res.getUpdatingPeriod() == req.getUpdatingPeriod() &&
                            res.getWatchdogPeriod() == req.getWatchdogPeriod() &&
                            res.getServerName().equals(req.getServerName()) &&
                            res.getPageOnServer().equals(req.getPageOnServer()) &&
                            res.getHostName().equals(req.getHostName());
                    checkSettingsCorrectness(correctSettings, settings.getMACAddress(), 
                            UPDATE_SERVER_CONFIG_DESCRIPTION);
            }
        };
        performUpdateAction(oldSettings, newSettings, new PutServerConfigRequestPacket(newSettings, password), 
                ResponsePacketType.SERVER_CONFIG, action, UPDATE_SERVER_CONFIG_DESCRIPTION, broadcastRequest, false);
    }
    
    public void updateStopServerConfig(DeviceSettings oldSettings, 
            DeviceSettings newSettings, int password, boolean broadcastRequest) 
            throws Exceptions.OperationFailed {
        UpdateAction action = new UpdateAction() {

            @Override
            public void perform(DatagramPacket p, DeviceSettings settings) throws Exceptions.OperationFailed, Exceptions.InvalidPacket {
                StopServerConfigResponsePacket scrp = new StopServerConfigResponsePacket(
                        makeStreamFromDatagram(p));
                StopServerConfig res = scrp.getStopServerConfig();
                StopServerConfig req = settings.getStopServerConfig();
                checkSettingsCorrectness(res.equals(req), settings.getMACAddress(), 
                        UPDATE_SERVER_CONFIG_DESCRIPTION);
            }
        };
        performUpdateAction(oldSettings, newSettings, new PutStopServerConfigRequestPacket(newSettings, password), 
                ResponsePacketType.DYNAMIC_DATA, action, UPDATE_SERVER_CONFIG_DESCRIPTION, broadcastRequest, false);
    }
    
    private static void handleTimeoutError(MACAddress address, String operationDescription) throws Exceptions.TimedOut {
        Exceptions.TimedOut exc = new Exceptions.TimedOut(operationDescription, address);
        logger.error(exc.getMessage());
        throw exc;
    }
    
    private synchronized static List<DatagramPacket> communicateWithTimeout(String operation, IPAddress destAddress, 
            RequestPacket packet, boolean singleReply) throws Exceptions.OperationFailed {
        List<DatagramPacket> result = new LinkedList<>();
        try {
            send(destAddress, packet);
            logger.info("Ожидание ответа");
            while (true) {
                DatagramPacket response = new DatagramPacket(new byte[MAX_PACKET_SIZE], 
                        MAX_PACKET_SIZE);
                socket.receive(response);
                result.add(response);
                logger.info("Получен пакет от {}", response.getAddress());
                if (singleReply) {
                    return result;
                }
            }
        }
        catch (SocketTimeoutException exc) {
            return result;
        }
        catch (IOException exc) {
            Exceptions.OperationFailed of = new Exceptions.OperationFailed(
                operation, exc.getMessage());
            logger.error(of.getMessage());
            throw of;
        }
    }
    
    private synchronized static void send(IPAddress destAddress, RequestPacket packet)
            throws IOException {
        byte[] data = packet.getData();
        DatagramPacket request = new DatagramPacket(data, data.length, 
                Inet4Address.getByName(destAddress.toString()), SERVER_SOCKET_PORT);
        logger.info("Отправлен {} на {}", packet.getRequestPacketType(), destAddress);
        socket.send(request);
    }
    
    public static UDPClient getClient() throws IllegalStateException {
        if (instance == null)
            throw new IllegalStateException("UDP-клиент не иницализирован");
        return instance;
    }
    
    public static void initialize() throws Exceptions.OperationFailed {
        try {
            if (instance == null) {
                instance = new UDPClient();
            }
        }
        catch (UnknownHostException | SocketException exc) {
            Exceptions.OperationFailed ex = new Exceptions.OperationFailed(
                INITIALIZATION_DESCRIPTION, exc.getMessage());
            logger.error(ex.getMessage());
            throw ex;
        }
    }
    
    private static boolean isExpectedDevice(RequestPacket req, ResponsePacket resp, IPAddress expectedIp, MACAddress expectedMac,
            InetAddress inetAddress, MACAddress mac, boolean checkIp) {
        IPAddress ip = new IPAddress(inetAddress.getAddress());
        return (checkIp ? ip.equals(expectedIp) : true) && 
                mac.equals(expectedMac) && req.getDeviceType() == resp.getDeviceType();
    }
    
    private static void checkSettingsCorrectness(boolean isCorrect, MACAddress mac, String description) 
            throws Exceptions.OperationFailed {
        if (!isCorrect) {
            Exceptions.OperationFailed exc = new Exceptions.OperationFailed(
                    description, 
                    "Настройки, полученные с устройства, не соответствуют заданным значениям");
            logger.error(exc.getMessage());
            throw exc;
        } 
        else {
            logger.info("Настройки успешно изменены на устройстве {}", mac);
        }
    }
    
    private static LittleEndianDataInputStream makeStreamFromDatagram(
            DatagramPacket p) {
        return new LittleEndianDataInputStream(new ByteArrayInputStream(p.getData()));
    }
    
    private static final String 
            INITIALIZATION_DESCRIPTION = "Инициализация",
            SEARCH_DESCRIPTION = "Поиск устройств",
            SYNCHRONYZE_DATETIME_SETTINGS_DESCRIPTION = "Обновление даты и времени",
            SYNCHRONYZE_SERVER_CONFIG_DESCRIPTION = "Обновление конфигурации сервера",
            UPDATE_SERVER_CONFIG_DESCRIPTION = "Обновление конфигурации сервера",
            UPDATE_NETWORK_SETTINGS_DESCRIPTION = "Обновление сетевых настроек",
            UPDATE_NVRAM_SETTINGS_DESCRIPTION = "Обновление настроек в NVRAM",
            UPDATE_DATE_TIME_SETTINGS_DESCRIPTION = "Обновление даты и времени",
            UPDATE_DYNAMIC_DATA_DESCRIPTION = "Обновление динамических данных",
            UPDATE_CREEPING_LINE_SETTINGS_DESCRIPTION = "Обновление режима работы устройства",
            // WRONG_PASSWORD_DESCRIPTION = "Неверный пароль",
            SYNCHRONIZE_DEVICE_SETTINGS_DESCRIPTION = "Получение настроек с устройства",
            FLASH_DEVICE = "Выполнить прошивку устройства";
    
    public static boolean CAN_UPDATE_FROM_SERVER;
    public static boolean IS_STOP_BOARD;
    public static boolean IS_MEGA;
}
package info.itline.controller;

public class DisplayBrightness implements Cloneable {
    public DisplayBrightness(int value) throws IllegalArgumentException {
        setValue(value);
    }
    
    public int getValue() {
        return value;
    }
    
    public final void setValue(int value) throws IllegalArgumentException {
//        if (!UDPClient.IS_MEGA) {
//            if (value < MIN || value > MAX)
//                throw new IllegalArgumentException(ILLEGAL_VALUE_DESCR);
//        }
        this.value = value;
    }
    
    int getValueForDevice() {
        return value * STEP;
    }
    
    static DisplayBrightness createFromDeviceValue(int v) {
//        if (!UDPClient.IS_MEGA) {
//            if (v < MIN * STEP || v > MAX * STEP /* || v % STEP != 0 */)
//                throw new IllegalArgumentException(ILLEGAL_VALUE_DESCR);
//        }
        return new DisplayBrightness(v / 11);
    }
    
    @Override
    public DisplayBrightness clone() {
        return new DisplayBrightness(value);
    }
    
    @Override
    public boolean equals(Object other) {
        if (!other.getClass().equals(this.getClass()))
            return false;
        return this.value == ((DisplayBrightness) other).value;
    }
    
    @Override
    public String toString() {
        return Integer.toString(value);
    }
    
    private int value;
    
    public static final int MIN = 0;
    public static final int MAX = 9;
    private static final int STEP = 11;
    
    private static final String ILLEGAL_VALUE_DESCR = "Недопустимое значение яркости";
}

package info.itline.binaryserializer;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface BinarySerializable {
    int byteCount() default 0;
}

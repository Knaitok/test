package info.itline.binaryserializer;

import info.itline.binaryserializer.BinarySerializer;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Serialize {
    int offset();
    int[] itemCount() default 0;
    int maxLength() default 0;
    int[] versions() default BinarySerializer.DEFAULT_VERSION;
}

package info.itline.binaryserializer;

public class PostCreateException extends RuntimeException {

    public PostCreateException(Throwable thrwbl) {
        super(thrwbl);
    }
}

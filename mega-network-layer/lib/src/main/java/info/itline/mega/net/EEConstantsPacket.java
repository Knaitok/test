package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;
import info.itline.binaryserializer.Serialize;

/**
 * Packet that represents the data about the EEConstants
 */
@BinarySerializable
public class EEConstantsPacket extends Packet {

    private static final int CONSTANTS_COUNT = 16;

    @Serialize(offset = 0, itemCount = CONSTANTS_COUNT)
    protected final short[] constants;

    private enum Fields {
        SIREN(0),
        TIME_SHOW_DURATION(1),
        DATE_SHOW_DURATION(2),
        TEMPERATURE_SHOW_DURATION(3),
        PRESSURE_SHOW_DURATION(4),
        HUMIDITY_SHOW_DURATION(5),
        RADIATION_SHOW_DURATION(6),
        STUB(7),
        USER_PASSWORD(8),
        ADMIN_PASSWORD(9);

        private final int index;

        private Fields(int index) {
            this.index = index;
        }

        public int getIndex() {
            return index;
        }
    }

    EEConstantsPacket() {
        this.constants = new short[CONSTANTS_COUNT];
    }

    EEConstantsPacket(short[] constants) {
        this.constants = constants;
    }

    private void postCreate() throws InvalidPacketException {
        checkType(PacketType.EE_CONSTANTS);
    }

    private short getField(Fields field) {
        return constants[field.getIndex()];
    }

    private void setField(Fields field, short value) {
        constants[field.getIndex()] = value;
    }

    public short getUserPassword() {
        return getField(Fields.USER_PASSWORD);
    }

    public void setUserPassword(short password) {
        setField(Fields.USER_PASSWORD, password);
    }

    public short getAdminPassword() {
        return getField(Fields.ADMIN_PASSWORD);
    }

    public void setAdminPassword(short password) {
        setField(Fields.ADMIN_PASSWORD, password);
    }
}

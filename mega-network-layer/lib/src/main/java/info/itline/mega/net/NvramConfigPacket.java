package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;
import info.itline.binaryserializer.Serialize;

@BinarySerializable
public class NvramConfigPacket extends Packet {

    NvramConfigPacket(int safe, short min, short max, 
            int watchdogPeriod, int autoBrightness) {
        this.safe = safe;
        this.min = min;
        this.max = max;
        this.watchdogPeriod = watchdogPeriod;
        this.autoBrightness = autoBrightness;
//        this.enableSiren = enableSiren;
        setPacketType(PacketType.NVRAM_CONFIG);
    }

    private NvramConfigPacket() {
        this.safe = 0;
        this.min = 0;
        this.max = 0;
        this.watchdogPeriod = 0;
        this.autoBrightness = 0;
//        this.enableSiren = 0;
    }

    private void postCreate() throws InvalidPacketException {
        checkType(PacketType.NVRAM_CONFIG);
    }
    
    public int getSafe() {
        return safe;
    }

    public short getMin() {
        return min;
    }

    public short getMax() {
        return max;
    }

    public int getWatchdogPeriod() {
        return watchdogPeriod;
    }

    public int getAutoBrightness() {
        return autoBrightness;
    }

//    public int getEnableSiren() {
//        return enableSiren;
//    }
    
    @Serialize(offset = 0)
    private final int safe;
    @Serialize(offset = 1)
    private final short min;
    @Serialize(offset = 2)
    private final short max;
    @Serialize(offset = 3)
    private final int watchdogPeriod;
    @Serialize(offset = 4)
    private final int autoBrightness;
//    @Serialize(offset = 5)
//    private final int enableSiren;
}

package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;
import info.itline.binaryserializer.Serialize;

@BinarySerializable
public class TimerConfig {

    private TimerConfig() {
        this.currentValue = 0;
        this.startValue = 0;
        this.stopValue = 0;
        this.flags = 0;
        this.state = 0;
        this.fields = 0;
    }

    public TimerConfig(int currentValue, int startValue, int stopValue,
            short flags, byte state, byte fields) {
        this.currentValue = currentValue;
        this.startValue = startValue;
        this.stopValue = stopValue;
        this.flags = flags;
        this.state = state;
        this.fields = fields;
    }

   public int getCurrentValue() {
      return currentValue;
   }

   public int getStartValue() {
      return startValue;
   }

   public int getStopValue() {
      return stopValue;
   }

   public short getFlags() {
      return flags;
   }

   public byte getState() {
      return state;
   }

   public byte getFields() {
      return fields;
   }

   public static short 
      AUTO_RESTART         = (short) (1 << 0),
      ENABLE_SIREN         = (short) (1 << 1),
      HIDE_TIMER           = (short) (1 << 2),
      HIDE_STOPPED         = (short) (1 << 3);
   
   public static short makeTimerFlags(boolean slave, int masterTimer, short... flags) {
      short result = 0;
      if (slave) {
         result = (short) ((1 << 4) & ((masterTimer & 0x5) << 5));
      }
      for (short f: flags) {
         result |= f;
      }
      return result;
   }
   
   public boolean isRunning() {
       return state == TimerState.RUNNING.getValue();
   }
   
   @Serialize(offset = 0)
   private final int currentValue;
   @Serialize(offset = 1)
   private final int startValue;
   @Serialize(offset = 2)
   private final int stopValue;
   @Serialize(offset = 3)
   private final short flags;
   @Serialize(offset = 4)
   private final byte state;
   @Serialize(offset = 5)
   private final byte fields;
}

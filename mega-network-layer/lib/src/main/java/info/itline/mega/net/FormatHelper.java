package info.itline.mega.net;

public class FormatHelper {

    private FormatHelper() {
    }
    
    static String formatAddress(byte[] bytes, String byteFormat, String separatorFormat) {
        StringBuilder res = new StringBuilder();
        String partFmt = byteFormat + separatorFormat;
        for (int i = 0; i < bytes.length - 1; i++) {
            res.append(String.format(partFmt, bytes[i] & 0xFF));
        }
        res.append(String.format(byteFormat, bytes[bytes.length - 1] & 0xFF));
        return res.toString();
    }
}

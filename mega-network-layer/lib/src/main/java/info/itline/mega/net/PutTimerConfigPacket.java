package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;

@BinarySerializable
public class PutTimerConfigPacket extends TimerConfigPacket {

    public PutTimerConfigPacket(short[] ids, TimerConfig[] configs) {
        super(ids, configs);
        setPacketType(PacketType.PUT_TIMER_CONFIG);
    }
    
}

package info.itline.mega.net;

import java.util.HashMap;
import java.util.Map;

public enum TimerConfigField {
    
    STATE                   ((byte)0x1),
    CURRENT_VALUE           ((byte)0x2),
    START_VALUE             ((byte)0x4),
    STOP_VALUE              ((byte)0x8),
    FLAGS                   ((byte)0x10),
    ALL                     ((byte) (
                                STATE.getValue() | 
                                CURRENT_VALUE.getValue() | 
                                START_VALUE.getValue() | 
                                STOP_VALUE.getValue() | 
                                FLAGS.getValue()));
    
    private TimerConfigField(byte value) {
        this.value = value;
    }

    public byte getValue() {
        return value;
    }
    
    static TimerConfigField getByValue(byte b) {
        return map.get(b);
    }
    
    private final byte value;

    private final static Map<Byte, TimerConfigField> map = new HashMap<>();
    static {
        for (TimerConfigField t: TimerConfigField.values()) {
            map.put(t.getValue(), t);
        }
    }
}
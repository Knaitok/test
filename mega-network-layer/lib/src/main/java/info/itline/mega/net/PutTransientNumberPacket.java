package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;

@BinarySerializable
class PutTransientNumberPacket extends TransientNumberPacket {

    PutTransientNumberPacket(short[] idx, int[] data) {
        super(idx, data);
        setPacketType(PacketType.PUT_TRANSIENT_NUMBER);
    }
}

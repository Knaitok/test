package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;
import info.itline.binaryserializer.Serialize;

@BinarySerializable
class GetTransientStringPacket extends Packet {

    public GetTransientStringPacket(byte[] idx) {
        int len = idx.length;
        int freeItemCount = TransientStringPacket.INDEX_COUNT - len;
        System.arraycopy(idx, 0, this.idx, 0, len);
        for (int i = 0; i < freeItemCount; i++) {
            this.idx[len + i] = InvalidItemIndex.BYTE_VALUE;
        }
        setPacketType(PacketType.GET_TRANSIENT_STRING);
    }

    public byte[] getIdx() {
        return idx;
    }
    
    @Serialize(offset = 0, itemCount = TransientStringPacket.INDEX_COUNT)
    private final byte[] idx = new byte[TransientStringPacket.INDEX_COUNT];
}

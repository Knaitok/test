package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;

@BinarySerializable
class PutNvramConfigPacket extends NvramConfigPacket {

    public PutNvramConfigPacket(int safe, short min, short max, 
            int watchdogPeriod, int autoBrightness) {
        super(safe, min, max, watchdogPeriod, autoBrightness);
        setPacketType(PacketType.PUT_NVRAM_CONFIG);
    }
}
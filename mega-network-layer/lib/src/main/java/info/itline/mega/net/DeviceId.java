package info.itline.mega.net;

import java.io.Serializable;
import java.util.Objects;

public class DeviceId implements Serializable {

    public DeviceId(DeviceType type, IpAddress ipAddress, MacAddress macAddress, String name) {
        this.name = name;
        this.type = type;
        this.ipAddress = ipAddress;
        this.macAddress = macAddress;
    }

    public DeviceId(DeviceInfoPacket p) {
        this(p.getDeviceType(), p.getIpAddress(), p.getMacAddress(), p.getName());
    }
    
    public DeviceType getType() {
        return type;
    }

    public IpAddress getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(IpAddress ipAddress) {
        this.ipAddress = ipAddress;
    }

    public MacAddress getMacAddress() {
        return macAddress;
    }

    public String getName() {
        return name;
    }
    
    @Override
    public String toString() {
        return name + " (" + macAddress.toString() + ")";
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.macAddress);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DeviceId other = (DeviceId) obj;
        if (!Objects.equals(this.macAddress, other.macAddress)) {
            return false;
        }
        return true;
    }
    
    private final DeviceType type;
    private IpAddress ipAddress;
    private final MacAddress macAddress;
    private String name;
    
    private static final long serialVersionUID = 1;
}

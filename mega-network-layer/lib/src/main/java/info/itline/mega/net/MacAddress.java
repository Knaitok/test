package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;
import info.itline.binaryserializer.Serialize;
import java.io.Serializable;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@BinarySerializable
public class MacAddress implements Serializable {
    
    private MacAddress() {
        bytes = null;
    }
    
    MacAddress(byte[] bytes) {
        this.bytes = new byte[SIZE];
        System.arraycopy(bytes, 0, this.bytes, 0, SIZE);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Arrays.hashCode(this.bytes);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MacAddress other = (MacAddress) obj;
        if (!Arrays.equals(this.bytes, other.bytes)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return FormatHelper.formatAddress(bytes, "%02x", ":");
    }

    public static MacAddress parse(String s) throws IllegalArgumentException {
        Matcher m = MAC_PATTERN.matcher(s);
        IllegalArgumentException exc = 
                new IllegalArgumentException("Строка не является MAC-адресом");
        if (!m.matches() || m.groupCount() != SIZE) {
            throw exc;
        }
        byte[] buf = new byte[SIZE];
        for (int i = 0; i < m.groupCount(); i++) {
            buf[i] = (byte) (Integer.parseInt(m.group(i + 1), 16) & 0xFF);
        }
        return new MacAddress(buf);
    }

    public byte[] getBytes() {
        byte[] result = new byte[SIZE];
        System.arraycopy(bytes, 0, result, 0, SIZE);
        return result;
    }

    public static boolean isAddress(String s) {
        return MAC_PATTERN.matcher(s).matches();
    }

    @Serialize(offset = 0, itemCount = SIZE)
    private final byte[] bytes;
    
    public static final int SIZE = 6;
    public static final MacAddress broadcastAddress;
    
    private static final Pattern MAC_PATTERN;
    static {
        String b = "([A-Fa-f0-9]{2})";
        String s = ":";
        MAC_PATTERN = Pattern.compile(
                "^" + b + s + b + s + b + s + b + s + b + s + b + "$");
        broadcastAddress = MacAddress.parse("FF:FF:FF:FF:FF:FF");
    }
    
    private static final long serialVersionUID = 1;
}

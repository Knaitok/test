package info.itline.mega.net.handlers;

import info.itline.mega.net.Packet;

public class NettyRequestHandler {
    private final Class<? extends Packet> packetClass;
    private final OnReceive onReceive;
    private final OnError onError;
    private final long sentTimestamp = System.currentTimeMillis();

    public NettyRequestHandler(Class<? extends Packet> packetClass, OnReceive<? extends Packet> onReceive,
                               OnError onError) {
        this.packetClass = packetClass;
        this.onReceive = onReceive;
        this.onError = onError;
    }

    public Class<? extends Packet> getPacketClass() {
        return packetClass;
    }

    public void fireOnReceive(Packet packet) {
        if (onReceive != null) onReceive.onReceive(packet);
    }

    public void fireOnError(String message) {
        if (onError != null) onError.onError(message);
    }

    public long getSentTimestamp() {
        return sentTimestamp;
    }
}

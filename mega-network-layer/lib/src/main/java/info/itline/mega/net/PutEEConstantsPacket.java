package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;

@BinarySerializable
public class PutEEConstantsPacket extends EEConstantsPacket {

    public PutEEConstantsPacket(EEConstantsPacket data) {
        super(data.constants);
        setPacketType(PacketType.PUT_EE_CONSTANTS);
    }

}

package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;
import info.itline.binaryserializer.Serialize;
import java.util.Calendar;
import java.util.GregorianCalendar;

@BinarySerializable
public class DateTimeSettingsPacket extends Packet{

    DateTimeSettingsPacket(int stamp) {
        this.stamp = stamp;
        setPacketType(PacketType.DATETIME_SETTINGS);
    }
    
    private DateTimeSettingsPacket() {
        this.stamp = convertCalendarToStamp(epoch);
    }
    
    private void postCreate() throws InvalidPacketException {
        checkType(PacketType.DATETIME_SETTINGS);
    }

    public int getStamp() {
        return stamp;
    }
    
    public static Calendar convertTimestampToCalendar(int stamp) {
        Calendar result = new GregorianCalendar(
                epoch.get(Calendar.YEAR), 
                epoch.get(Calendar.MONTH), 
                epoch.get(Calendar.DAY_OF_MONTH));
        result.add(Calendar.SECOND, stamp);
        return result;
    }
    
    public static int convertCalendarToStamp(Calendar c) {
        return (int) ((c.getTime().getTime() - epoch.getTime().getTime()) / 1000);
    }
    
    private static final Calendar epoch = new GregorianCalendar(2000, 0, 1);
    
    @Serialize(offset = 0)
    private final int stamp;
}

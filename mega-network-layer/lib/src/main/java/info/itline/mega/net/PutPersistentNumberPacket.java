package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;

@BinarySerializable
class PutPersistentNumberPacket extends PersistentNumberPacket {

    public PutPersistentNumberPacket(short[] idx, int[] data) {
        super(idx, data);
        setPacketType(PacketType.PUT_PERSISTENT_NUMBER);
    }
}

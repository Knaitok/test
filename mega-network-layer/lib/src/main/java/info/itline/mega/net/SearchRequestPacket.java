package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;

@BinarySerializable
class SearchRequestPacket extends Packet {

    SearchRequestPacket() {
        setPacketType(PacketType.SEARCH);
        setWaitForMultipleResponses(true);
    }
}

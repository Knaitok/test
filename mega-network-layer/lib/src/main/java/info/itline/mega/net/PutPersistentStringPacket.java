package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;

@BinarySerializable
class PutPersistentStringPacket extends PersistentStringPacket {

    PutPersistentStringPacket(byte idx, byte profile, String string) {
        super(idx, profile, string);
        setPacketType(PacketType.PUT_PERSISTENT_STRING);
    }
    
    private void postCreate() throws InvalidPacketException {
        checkType(PacketType.PUT_PERSISTENT_STRING);
    }
}

package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;
import info.itline.binaryserializer.Serialize;

@BinarySerializable
public class ModePacket extends Packet{
    
    private void postCreate() throws InvalidPacketException {
        checkType(PacketType.MODE);
    }

    public int getCurrentMode() {
        return currentMode;
    }

    public int getOldMode() {
        return oldMode;
    }
    
    @Serialize(offset = 0)
    private int currentMode;
    @Serialize(offset = 1)
    private int oldMode;
}

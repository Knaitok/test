package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;

@BinarySerializable
class PutNvramSettingsPacket extends NvramSettingsPacket {

    public PutNvramSettingsPacket(int brightness) {
        super(brightness);
        setPacketType(PacketType.PUT_NVRAM_SETTINGS);
    }
}

package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;
import info.itline.binaryserializer.Serialize;

@BinarySerializable
public class GetTimerConfigPacket extends Packet {
    
    public GetTimerConfigPacket(short[] ids) {
        setPacketType(PacketType.GET_TIMER_CONFIG);
        
        final int len = TimerConfigPacket.TIMER_COUNT;
        assert(ids.length <= len);
        
        this.ids = new byte[len];
        for (int i = 0; i < ids.length; i++) {
            this.ids[i] = (byte)ids[i];
        }
        for (int i = 0; i < len - ids.length; i++) {
            this.ids[ids.length + i] = InvalidItemIndex.BYTE_VALUE;
        }
    }

    public byte[] getIds() {
        return ids;
    }
    
    @Serialize(offset = 0, itemCount = TimerConfigPacket.TIMER_COUNT)
    private final byte[] ids;
}

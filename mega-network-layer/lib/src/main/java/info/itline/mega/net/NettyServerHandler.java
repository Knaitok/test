package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializer;
import info.itline.binaryserializer.PostCreateException;

import info.itline.mega.net.handlers.NettyRequestHandler;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.channel.socket.DatagramPacket;
import io.netty.util.CharsetUtil;
import io.netty.util.ReferenceCountUtil;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.io.IOException;
import java.util.Map;


/**
 * Handles a server-side channel.
 */
public class NettyServerHandler extends SimpleChannelInboundHandler<DatagramPacket> { // (1)
    private final BinarySerializer serializer = new BinarySerializer("windows-1251");
    private int packetId = 0;
    static final ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    String name = "FakeDevice";
    IpAddress ipAddress = IpAddress.parse("127.0.0.1");
    IpAddress netmask = IpAddress.parse("255.255.255.0");
    IpAddress gateway = IpAddress.parse("127.0.0.1");
    short version = 0;
    short productionDate= 0;
    MacAddress macAddress = new MacAddress(new byte[] {1, 2, 3, 4, 5, 6});
    PacketType packetType = PacketType.DEVICE_INFO;
    DeviceType deviceType = DeviceType.MEGA_BOARD;
    int id = 0;
    boolean waitForMultipleResponses = false;
    int currentMode = 0;
    int oldMode = 1;
    @Override

    public void channelRead0(ChannelHandlerContext ctx, DatagramPacket msg) { // (2)

        System.out.println(msg);

        byte[] bytes = new byte[msg.content().readableBytes()];

        try {

            msg.content().getBytes(0, bytes);
            Packet packet = serializer.deserialize(Packet.class, bytes);
            System.out.println(packet.getPacketType());

            if(packet.getPacketType() == PacketType.SEARCH)
                handleSearchDeviceRequest(ctx, msg);

        } catch (PostCreateException |IOException e) {

        } finally {

        }
    }


    private void handleSearchDeviceRequest(ChannelHandlerContext ctx, DatagramPacket msg){

        try {
            DeviceInfoPacket di = new DeviceInfoPacket();
            di.setName(name);
            di.setIpAddress(ipAddress);
            di.setGateway(gateway);
            di.setNetmask(netmask);
            di.setVersion(version);
            di.setProductionDate(productionDate);
            di.setMacAddress(macAddress);
            di.setPacketType(packetType);
            di.setDeviceType(deviceType);
            di.setPacketId(id);
            di.setWaitForMultipleResponses(waitForMultipleResponses);

            byte[] bytes = serializer.serialize(di);
            System.out.println(msg.sender());
            ctx.write(new DatagramPacket(Unpooled.copiedBuffer(bytes), msg.sender()));
            ctx.flush();
        }

        catch (Exception e) {

        }
    }


    @Override

    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) { // (4)
        cause.printStackTrace();
        ctx.close();
    }
}
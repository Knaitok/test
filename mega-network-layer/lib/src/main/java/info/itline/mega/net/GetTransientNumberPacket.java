package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;

@BinarySerializable
class GetTransientNumberPacket extends GetPersistentNumberPacket {

    public GetTransientNumberPacket(short[] idx) {
        super(idx);
        setPacketType(PacketType.GET_TRANSIENT_NUMBER);
    }
}

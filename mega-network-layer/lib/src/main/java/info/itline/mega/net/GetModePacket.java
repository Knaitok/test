package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;

@BinarySerializable
class GetModePacket extends Packet {

    GetModePacket() {
        setPacketType(PacketType.GET_MODE);
    }
}

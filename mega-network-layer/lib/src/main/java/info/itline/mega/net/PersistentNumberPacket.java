package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;
import info.itline.binaryserializer.Serialize;

@BinarySerializable
public class PersistentNumberPacket extends Packet {

    PersistentNumberPacket(short[] idx, int[] data) {
        assert(idx.length == data.length);
        assert(idx.length <= INDEX_COUNT);
        
        int len = idx.length;
        int nEmpty = INDEX_COUNT - len;
        
        this.idx = new short[INDEX_COUNT];
        System.arraycopy(idx, 0, this.idx, 0, len);
        for (int i = 0; i < nEmpty; i++) {
            this.idx[len + i] = InvalidItemIndex.SHORT_VALUE;
        }
        
        this.data = new int[INDEX_COUNT];
        System.arraycopy(data, 0, this.data, 0, len);
        setPacketType(PacketType.PERSISTENT_NUMBER);
    }

    private  PersistentNumberPacket() {
        this.idx = null;
        this.data = null;
    }
    
    private void postCreate() throws InvalidPacketException {
        checkType(PacketType.PERSISTENT_NUMBER);
    }
    
    public short[] getIndexes() {
        return idx;
    }
    
    public int[] getValues() {
        return data;
    }
    
    @Serialize(offset = 0, itemCount = INDEX_COUNT)
    private final short[] idx;
    @Serialize(offset = 1, itemCount = INDEX_COUNT)
    private final int[] data;
    
    public static final int INDEX_COUNT = 128;
}

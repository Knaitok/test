package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;
import info.itline.binaryserializer.Serialize;

@BinarySerializable
class PutModePacket extends Packet {

    public PutModePacket(int mode) {
        this.mode = mode;
        setPacketType(PacketType.PUT_MODE);
    }

    public int getMode() {
        return mode;
    }
    
    @Serialize(offset = 0)
    private final int mode;
}

package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;
import info.itline.binaryserializer.Serialize;

@BinarySerializable
public class TimerConfigPacket extends Packet {

    TimerConfigPacket(short[] ids, TimerConfig[] configs) {
        
        setPacketType(PacketType.TIMER_CONFIG);
        
        assert(ids.length == configs.length);
        assert(ids.length <= TIMER_COUNT);
        
        this.ids = new short[TIMER_COUNT];
        System.arraycopy(ids, 0, this.ids, 0, ids.length);
        for (int i = 0; i < TIMER_COUNT - ids.length; i++) {
            this.ids[ids.length + i] = InvalidItemIndex.SHORT_VALUE;
        }
        
        this.configs = ArrayHelper.copyAndFill(configs, TIMER_COUNT, null);
    }

    public TimerConfigPacket() {
        this.ids = null;
        this.configs = null;
    }
    
    private void postCreate() throws InvalidPacketException {
        checkType(PacketType.TIMER_CONFIG);
    }

    public short[] getIds() {
        return ids;
    }

    public TimerConfig[] getConfigs() {
        return configs;
    }
    
    @Serialize(offset = 0, itemCount = TIMER_COUNT)
    private final short[] ids;
    @Serialize(offset = 1, itemCount = TIMER_COUNT)
    private final TimerConfig[] configs;
    
    public static final int TIMER_COUNT = 64;
}

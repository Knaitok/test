package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;

@BinarySerializable
class GetDeviceInfoPacket extends Packet {

    public GetDeviceInfoPacket() {
        setPacketType(PacketType.GET_DEVICE_INFO);
    }
}
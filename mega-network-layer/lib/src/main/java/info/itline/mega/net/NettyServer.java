package info.itline.mega.net;

import info.itline.mega.net.handlers.NettyRequestHandler;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;

import java.util.HashMap;
import java.util.Map;


public class NettyServer {
    private static final int SOCKET_PORT = 5001;
    private int port;
    private final Map<Integer, NettyRequestHandler> requests = new HashMap<>();


    public NettyServer() {
        this.port = SOCKET_PORT;
    }


    public void run() throws Exception {
        EventLoopGroup workerGroup;
        workerGroup = new NioEventLoopGroup();
        Bootstrap bootstrap = new Bootstrap();
        try {
            bootstrap.group(workerGroup)
                    .channel(NioDatagramChannel.class)
                    .handler(new NettyServerHandler());
            bootstrap.bind(port).sync().channel().closeFuture().await();
        } catch (InterruptedException e) {
            workerGroup.shutdownGracefully();
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) throws Exception {
        new NettyServer().run();

    }
}
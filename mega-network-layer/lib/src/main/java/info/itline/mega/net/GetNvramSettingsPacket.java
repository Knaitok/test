package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;

@BinarySerializable
class GetNvramSettingsPacket extends Packet {

    public GetNvramSettingsPacket() {
        setPacketType(PacketType.GET_NVRAM_SETIINGS);
    }
}

package info.itline.mega.net;

public class InvalidPacketException extends Exception {

    public InvalidPacketException() {
    }

    public InvalidPacketException(String string) {
        super(string);
    }

    public InvalidPacketException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public InvalidPacketException(Throwable thrwbl) {
        super(thrwbl);
    }
}

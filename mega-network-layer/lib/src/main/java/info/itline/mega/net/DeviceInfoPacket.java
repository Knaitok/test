package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;
import info.itline.binaryserializer.Serialize;

@BinarySerializable
public class DeviceInfoPacket extends Packet {

    public String getName() {
        return name;
    }

    public IpAddress getIpAddress() {
        return ipAddress;
    }

    public IpAddress getNetmask() {
        return netmask;
    }

    public IpAddress getGateway() {
        return gateway;
    }

    public short getVersion() {
        return version;
    }

    public short getProductionDate() {
        return productionDate;
    }

    public void setName(String name) {this.name = name;}

    public void setIpAddress(IpAddress ipAddress) {this.ipAddress = ipAddress;}

    public void setNetmask(IpAddress netmask) {this.netmask = netmask;}

    public void setGateway(IpAddress gateway) {this.gateway = gateway;}

    public void setVersion(short version) {this.version = version;}

    public void setProductionDate(short date) {this.productionDate = date;}
    
    private void postCreate() throws InvalidPacketException {
        checkType(PacketType.DEVICE_INFO);
    }


    @Serialize(offset = 0, maxLength = NetworkSettingsPacket.NAME_LENGTH)
    private String name;
    @Serialize(offset = 1)
    private IpAddress ipAddress;
    @Serialize(offset = 2)
    private IpAddress netmask;
    @Serialize(offset = 3)
    private IpAddress gateway;
    
    @Serialize(offset = 4)
    private short version;
    @Serialize(offset = 5)
    private short productionDate;
}

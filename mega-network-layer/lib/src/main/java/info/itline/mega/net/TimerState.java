package info.itline.mega.net;

import info.itline.binaryserializer.FromInteger;
import info.itline.binaryserializer.ToInteger;
import java.util.HashMap;
import java.util.Map;

public enum TimerState {

    STOPPED     ((byte)0),
    RUNNING     ((byte)1),
    RESTART     ((byte)2);
    
    private TimerState(byte value) {
        this.value = value;
    }

    public byte getValue() {
        return value;
    }

    @FromInteger
    static TimerState getByValue(byte b) {
        return map.get(b);
    }
    
    private final byte value;

    private final static Map<Byte, TimerState> map = new HashMap<>();
    static {
        for (TimerState t: TimerState.values()) {
            map.put(t.getValue(), t);
        }
    }
}

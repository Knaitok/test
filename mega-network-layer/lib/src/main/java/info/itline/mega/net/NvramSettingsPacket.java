package info.itline.mega.net;

import info.itline.binaryserializer.BinarySerializable;
import info.itline.binaryserializer.Serialize;

@BinarySerializable
public class NvramSettingsPacket extends Packet {

    NvramSettingsPacket(int brightness) {
        this.brightness = brightness;
        setPacketType(PacketType.NVRAM_SETTINGS);
    }

    private NvramSettingsPacket() {
        brightness = 0;
    }
    
    public int getBrightness() {
        return brightness;
    }
    
    private void postCreate() throws InvalidPacketException {
        checkType(PacketType.NVRAM_SETTINGS);
    }
    
    @Serialize(offset = 1)
    private final int brightness;
    
    public static final int MIN_BRIGHTNESS = 0, MAX_BRIGHTNESS = 100;
}

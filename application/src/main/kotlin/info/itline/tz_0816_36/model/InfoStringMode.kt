package info.itline.tz_0816_36.model

import info.itline.editor.domain.Format

/**
 * This is class container for types of message string on the device.
 */
enum class InfoStringMode(val typeName: String, val typeFormat: Format) {
    DYNAMIC_FAST("Бегущая строка(быстро)", Format.STR_HIGH_SPEED),
    DYNAMIC_MEDIUM("Бегущая строка(средне)", Format.STR_MEDIUM_SPEED),
    DYNAMIC_LOW("Бегущая строка(медленно)", Format.STR_LOW_SPEED),
    STATIC("Статическая строка", Format.STR_LSCROLL);

    override fun toString(): String {
        return typeName
    }
}

package info.itline.tz_0816_36.model

/**
 * This is class container for sizes of text on the device in px.
 */
enum class FontSize(val number: Int, val fontSizeName: String) {
    FONT_8(2, "8 точек"),
    FONT_12(4, "12 точек"),
    FONT_16(7, "16 точек");

    companion object {
        fun from(value: Int): FontSize {
            return FontSize.values().first { it.number == value }
        }

        fun from(value: String): FontSize {
            return FontSize.values().first { it.fontSizeName == value }
        }

        /**
         * This field contains a regular expression that deals with the formatting
         */
        private val stringFormat = Regex("^#(\\d)f(.*)$")

        /**
         * Extracts the font and string elements from the passed value
         */
        fun extractFontAndString(str: String): Pair<FontSize, String> {
            val result = stringFormat.find(str) ?: return Pair(FontSize.FONT_8, str)
            val groups = result.groups
            if(groups.size == 3) {
                val int = groups[1]!!.value.toInt()
                return Pair(FontSize.from(int), groups[2]!!.value)
            }
            return Pair(FontSize.FONT_12, str)
        }
    }

    override fun toString(): String {
        return fontSizeName
    }

    /**
     * Format passed string according to the needed format
     */
    fun formatString(string: String): String {
        return "#${number}f${string}"
    }
}

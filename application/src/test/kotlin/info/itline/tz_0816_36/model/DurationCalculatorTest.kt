package info.itline.tz_0816_36.model

import org.junit.Test
import java.time.LocalDate

class DurationCalculatorTest {
    @Test
    fun convertMillenniumStart() {
        val date = LocalDate.of(2000, 1, 1)
        assert(0 == DurationCalculator.convertDateToInstant(date))
    }

    @Test
    fun convertDateToInterval() {
        val date = LocalDate.of(2000, 1, 5)
        val interval = 4 * DurationCalculator.SECONDS_IN_DAY
        assert(interval == DurationCalculator.convertDateToInstant(date))
    }

    @Test
    fun convertLongDateToInterval() {
        val date = LocalDate.of(2000, 2, 1)
        val interval = 31 * DurationCalculator.SECONDS_IN_DAY
        assert(interval == DurationCalculator.convertDateToInstant(date))
    }

    @Test
    fun convertIntervalToMillenniumStart() {
        assert(DurationCalculator.MILLENNIUM_START == DurationCalculator.convertInstantToDate(0))
    }

    @Test
    fun convertIntervalToDate() {
        val interval = 10 * DurationCalculator.SECONDS_IN_DAY
        val date = LocalDate.of(2000, 1, 11)
        assert(date == DurationCalculator.convertInstantToDate(interval))
    }

    @Test
    fun convertLongIntervalToDate() {
        val interval = 600 * DurationCalculator.SECONDS_IN_DAY
        val date = LocalDate.of(2001, 8, 23)
        assert(date == DurationCalculator.convertInstantToDate(interval))
    }

    @Test
    fun calculateDuration() {
        val offset = "100"
        val prevDate = LocalDate.now().minusDays(offset.toLong())
        assert(offset == DurationCalculator.calculateDuration(prevDate))
    }
}